package GUI;

import javax.swing.JFrame;

/**
 * 
 * @author TOM-12  <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class Frame extends JFrame{
    
    MainMenu mainMenu;
    /**
     *  Constructs frame for application/game
     * sets size,position,title
     * initialises main menu panel
     */
    public Frame(){      
                
        
                       
        setSize(250, 250);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setEnabled(true);
        setTitle("Gra w szachy");        
        mainMenu = new MainMenu(this);
    }

    

}


 