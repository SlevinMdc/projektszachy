package GUI;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import org.FileSave;
import org.GameBoardLogic;


/**
 *
 * @author TOM-12  <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class GameMenuBar extends JPanel {
    
    MainMenu mainMenu;
    JMenuBar menuBar;
    JMenu menu;
    /**
     * Creates menu bar for single player game
     * @param frame parent frame
     * @param parent parent Jpanel to which it belongs
     * @param gameBoard gameBoard instance
     */
    
    /**
     * Creates menu bar for single player game
     * @param frame parent frame
     * @param parent parent Jpanel to which it belongs
     * @param gameBoard gameBoard instance
     * @param save true if game mode supports saving(only hot seat at the moment)
     */
    public GameMenuBar(final JFrame frame,final JPanel parent,final GameBoardLogic gameBoard,boolean save){
        setLayout(new FlowLayout(FlowLayout.LEFT));
        
        menuBar = new JMenuBar();        
        menu = new JMenu("Gra");        
        JMenuItem nowaGra = new JMenuItem("Nowa Gra");
        nowaGra.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                gameBoard.resetBoard();
                
            }
        });
        menu.add(nowaGra);
                
        if (save == true) {
            JMenuItem saveItem = new JMenuItem("Zapisz");
            saveItem.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    if(!gameBoard.isEnd()){
                        FileSave save = new FileSave(frame,gameBoard);
                    }                    
                }
            });
            menu.add(saveItem);
        }
        JMenuItem wrocDoMenu = new JMenuItem("Wróć do menu");
        wrocDoMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            frame.remove(parent);
            mainMenu = new MainMenu(frame);
            }
        });
        menu.add(wrocDoMenu);
        
        JMenuItem zakoncz = new JMenuItem("Wyjście");
        zakoncz.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            System.exit(0);
            }
        });
        menu.add(zakoncz);  
        menuBar.add(menu);
        
        add(menuBar);
        
        
    }

}
