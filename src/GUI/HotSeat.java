package GUI;

import java.awt.AWTException;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import org.Parameters;

/**
 * 
 * @author TOM-12  <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class HotSeat extends JPanel{
    
    
    final GameMenuBar singleMenuBar;
    final HotSeatBoard gameBoard;
    private final LogDisplayText logDisplay;
    private final LogDisplayContainer logContainer;
    final TimerPanel timer;
    
    /**
     *  Creates Jpanel for single player game
     * @param jFrame parent JFrame to which it belongs
     * @param panel Jpanel from which was created/initialised (MainMenu)
     * @throws java.awt.AWTException
     * @throws java.lang.InterruptedException
     */
    public HotSeat(JFrame jFrame,JPanel panel) throws AWTException, InterruptedException{
                
        JPanel sidePanel=new JPanel();
        sidePanel.setLayout(new BoxLayout(sidePanel,BoxLayout.Y_AXIS));
        
        String lang="en";
        
        logDisplay = new LogDisplayText();
        logContainer = new LogDisplayContainer(logDisplay); 
        timer = new TimerPanel(lang,0,5,0,false);   
        Parameters param = new Parameters("en",logDisplay,timer,"down");
        gameBoard = new HotSeatBoard(param);
        singleMenuBar = new GameMenuBar(jFrame,this,gameBoard.gameBoardLogic,true);
        
        
        singleMenuBar.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        gameBoard.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        logContainer.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        timer.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        sidePanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        
        
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        c.fill = GridBagConstraints.HORIZONTAL;        
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 0;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.gridx = 0;
        c.gridy = 0;        
        add(singleMenuBar, c);
        
        c.fill = GridBagConstraints.NONE; 
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 1;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 1;        
        add(gameBoard, c);
                
        sidePanel.add(timer);
        sidePanel.add(logContainer);
        
        c.fill = GridBagConstraints.BOTH; 
        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 0;
        c.weighty = 1;
        c.gridwidth = 1;
        c.gridheight=1;
        c.gridx = 1;
        c.gridy = 1;        
        add(sidePanel, c);
        
        
        finalizeInit(jFrame,panel);
        
    }
    final void finalizeInit(JFrame jFrame, JPanel panel){
        setEnabled(true);
        setVisible(true);        
        jFrame.remove(panel);
        jFrame.add(this);   
        jFrame.validate();
        jFrame.pack();
        jFrame.setLocationRelativeTo(null);
        jFrame.setMinimumSize(jFrame.getSize());
    }
}
