package GUI;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.GameBoardLogic;
import org.Parameters;
import org.SaveFileData;
import pieces.Piece;

/**
 *
 * @author TOM-12 <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class HotSeatBoard extends JPanel implements MouseListener, MouseMotionListener {

    private static final String COLS = " ABCDEFGH ";

    /**
     * array of panels on chesboard
     */
    public JPanel square[][] = new JPanel[8][8];
    private JPanel chessBoard;
    private int xAdjustment;
    private int yAdjustment;
    private JLabel chessPiece;
    private JLayeredPane layeredPane;

    private Container originalContainer;
    /**
     * variable for core 
     */
    public final GameBoardLogic gameBoardLogic;
    private final Dimension boardSize;
    public int size=500;
    /**
     * state of game
     */
    public boolean endGame=false;
    
    private TimerPanel timer;
            
    private int blackTurn=1;
    private int whiteTurn=1;
    public String turn="WHITE";

   
    
    /**
     * Creates Board for single player game
     * @param par
     * @throws java.awt.AWTException
     * @throws java.lang.InterruptedException
     */
    public HotSeatBoard(Parameters par) throws AWTException, InterruptedException { 
        if(par.isTimer()){
            timer=par.getTimer();
        }
        final HotSeatBoard aThis = this;
        par.setBoard(aThis,"HotSeatBoard");
        
        gameBoardLogic = new GameBoardLogic(par);
        boardSize = new Dimension(size, size);
        initBoard();
    }
    
    public HotSeatBoard(Parameters par,SaveFileData data) throws AWTException, InterruptedException { 
        if(par.isTimer()){
            timer=par.getTimer();
        }
        final HotSeatBoard aThis = this;
        par.setBoard(aThis,"HotSeatBoard");
        turn=data.getWhosTurn();
        gameBoardLogic = new GameBoardLogic(par);
        boardSize = new Dimension(size, size);
        loadBoard();
        gameBoardLogic.setBoard(data.getTurn(),data.getTurns(),data.getCell(),data.getPieces()); 
    }
    
    
    /**
     * Initiates/creates nottion border and chessboard
     */
    private void initBoard(){
        setLayout(new BorderLayout());
        createBorderNotation();
        createBoard();
        gameBoardLogic.resetBoard();        
    }
    
    private void loadBoard(){
        setLayout(new BorderLayout());
        createBorderNotation();
        createBoard();               
    }
    
    
    
    
    
    
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(endGame==true){
            return;
        }
        chessPiece = null;
        Component c = chessBoard.findComponentAt(e.getX(), e.getY());

        if (c instanceof JPanel) {
            return;
        }
        if(  !((Piece) c).getPieceColor().equalsIgnoreCase(turn.substring(0, 1)) ){
            return;
        } 
        
        if (timer!=null && whiteTurn==1){
            if(((Piece) c).getPieceColor().equalsIgnoreCase("w")){
                timer.whiteStart();
            }
            if(((Piece) c).getPieceColor().equalsIgnoreCase("b")){
                timer.blackStart();
            }
        }
        originalContainer = c.getParent();
        gameBoardLogic.getAvailableSquares(c);
        gameBoardLogic.highlightValidMoves(c);
        Point parentLocation = c.getParent().getLocation();
        xAdjustment = parentLocation.x - e.getX();
        yAdjustment = parentLocation.y - e.getY();
        chessPiece = (JLabel) c;
        chessPiece.setLocation(e.getX() + xAdjustment, e.getY() + yAdjustment);
        layeredPane.add(chessPiece, JLayeredPane.DRAG_LAYER);
        layeredPane.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(endGame==true){
            return;
        }
        layeredPane.setCursor(null);

        if (chessPiece == null) {
            return;
        }

        //  The drop location should be within the bounds of the chess board
        int xMax = layeredPane.getWidth() - chessPiece.getWidth();
        int x = Math.min(e.getX(), xMax);
        x = Math.max(x, 0);

        int yMax = layeredPane.getHeight() - chessPiece.getHeight();
        int y = Math.min(e.getY(), yMax);
        y = Math.max(y, 0);

        Component c = chessBoard.findComponentAt(x, y);
        Container destinationContainer;
        if (c instanceof JLabel) {
            destinationContainer = c.getParent();
        } else {
            destinationContainer = (Container) c;
        }

            
        boolean move;
        move=gameBoardLogic.makeMove(chessPiece, layeredPane, originalContainer, destinationContainer);
        gameBoardLogic.unHighlightAllMoves();
        if(move==true){
            if(turn.equalsIgnoreCase("WHITE")){
                turn="BLACK";
                if (timer!=null){
                    timer.whiteStop();
                    timer.blackStart();
                }
            }else{
                if(turn.equalsIgnoreCase("BLACK")){
                    turn="WHITE";
                    if (timer!=null){
                        timer.whiteStart();
                        timer.blackStop();
                    }
                }
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent me) {
        if(endGame==true){
            return;
        }
        if (chessPiece == null) {
            return;
        }

        //  The drag location should be within the bounds of the chess board
        int x = me.getX() + xAdjustment;
        int xMax = layeredPane.getWidth() - chessPiece.getWidth();
        x = Math.min(x, xMax);
        x = Math.max(x, 0);

        int y = me.getY() + yAdjustment;
        int yMax = layeredPane.getHeight() - chessPiece.getHeight();
        y = Math.min(y, yMax);
        y = Math.max(y, 0);

        chessPiece.setLocation(x, y);
    }

    @Override
    public void mouseMoved(MouseEvent e) {}

    /**
     * Creates notation around board(1-8 A-H)
     */
    private void createBorderNotation() {

        JPanel pageStartRow = new JPanel();
        pageStartRow.setPreferredSize(new Dimension((boardSize.width), (boardSize.height / 8)));
        pageStartRow.setLayout(new BoxLayout(pageStartRow, BoxLayout.X_AXIS));
        for (int i = 0; i <= 9; ++i) {
            JLabel label = new JLabel(COLS.substring(i, i + 1), SwingConstants.CENTER);
            label.setMaximumSize(new Dimension((boardSize.width / 8), (boardSize.height / 8)));
            pageStartRow.add(label);
        }
        add(pageStartRow, BorderLayout.PAGE_START);

        JPanel pageEndRow = new JPanel();
        pageEndRow.setPreferredSize(new Dimension((boardSize.width), (boardSize.height / 8)));
        pageEndRow.setLayout(new BoxLayout(pageEndRow, BoxLayout.X_AXIS));
        for (int i = 0; i <= 9; ++i) {
            JLabel label = new JLabel(COLS.substring(i, i + 1), SwingConstants.CENTER);
            label.setMaximumSize(new Dimension((boardSize.width / 8), (boardSize.height / 8)));
            pageEndRow.add(label);
        }
        add(pageEndRow, BorderLayout.PAGE_END);

        JPanel lineStartCol = new JPanel();
        lineStartCol.setPreferredSize(new Dimension((boardSize.width / 8), (boardSize.height)));
        lineStartCol.setLayout(new BoxLayout(lineStartCol, BoxLayout.Y_AXIS));
        for (int i = 0; i <= 7; ++i) {
            JLabel label = new JLabel(Integer.toString(8 - i), SwingConstants.CENTER);
            label.setMaximumSize(new Dimension((boardSize.width / 8), (boardSize.height / 8)));
            lineStartCol.add(label);
        }
        add(lineStartCol, BorderLayout.LINE_START);

        JPanel lineEndCol = new JPanel();
        lineEndCol.setPreferredSize(new Dimension((boardSize.width / 8), (boardSize.height)));
        lineEndCol.setLayout(new BoxLayout(lineEndCol, BoxLayout.Y_AXIS));
        for (int i = 0; i <= 7; ++i) {
            JLabel label = new JLabel(Integer.toString(8 - i), SwingConstants.CENTER);
            label.setMaximumSize(new Dimension((boardSize.width / 8), (boardSize.height / 8)));
            lineEndCol.add(label);
        }
        add(lineEndCol, BorderLayout.LINE_END);
    }
    
    
    /**
     * Moves piece on board if possible and returns true if executed
     * @param jPanelSource  panel on which piece is currently  square[row][col]
     * @param jPanelDestination panel to which move piece  square[row][col]
     * @return true if was executed and legal move   false if moc\ve is illegal or/and not executed
     */
    private boolean movePiece(JPanel jPanelSource, JPanel jPanelDestination){
        chessPiece = null;
        if(jPanelSource.getComponentCount()!=1){
            return false;
        }
        
        Component c = jPanelSource.findComponentAt(10, 10);
        chessPiece = (JLabel) c;
        originalContainer = c.getParent();
        gameBoardLogic.getAvailableSquares(c);
        gameBoardLogic.highlightValidMoves(c);
        layeredPane.add(chessPiece, JLayeredPane.DRAG_LAYER);
        
        
        
        Component d = jPanelDestination.findComponentAt(10, 10);
        Container destinationContainer;
        if (d instanceof JLabel) {
            destinationContainer = d.getParent();
        } else {
            destinationContainer = (Container) d;            
        }
        
        gameBoardLogic.makeMove(chessPiece, layeredPane, originalContainer, destinationContainer);
        gameBoardLogic.unHighlightAllMoves();
        return true;
    }

    private void createBoard() {
        layeredPane = new JLayeredPane();
        layeredPane.setPreferredSize(boardSize);
        layeredPane.addMouseListener(this);
        layeredPane.addMouseMotionListener(this);
        layeredPane.setEnabled(false);
        add(layeredPane, BorderLayout.CENTER);

        chessBoard = new JPanel();
        chessBoard.setLayout(new GridLayout(8, 8));
        chessBoard.setPreferredSize(boardSize);
        chessBoard.setBounds(0, 0, boardSize.width, boardSize.height);
        layeredPane.add(chessBoard, JLayeredPane.DEFAULT_LAYER);

        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                square[i][j] = new JPanel();
                square[i][j].setOpaque(true);
                //square[i][j].setPreferredSize(new Dimension(50, 50));
                square[i][j].setMinimumSize(new Dimension(size/8, size/8));
                square[i][j].setBackground((i + j) % 2 == 0 ? Color.white : Color.gray);
                square[i][j].setBorder(null);
                square[i][j].setName(COLS.substring(j + 1, j + 2).toLowerCase() + "" + (8 - i));
                chessBoard.add(square[i][j]);
            }
        }
    }

    /**
     * Reset turns
     */
    public void resetTurns(){
        turn="WHITE";
        whiteTurn=1;
        blackTurn=1;
    }
    
    
    /**
     * Clears chess board
     */
    public void clearBoard(){
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                square[i][j].removeAll();
            }
        }
        layeredPane.removeAll();
        layeredPane.add(chessBoard, JLayeredPane.DEFAULT_LAYER);
    }
}
