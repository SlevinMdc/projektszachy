package GUI;

import java.awt.AWTException;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.Parameters;
import org.SaveFileData;

/**
 *
 * @author TOM-12 <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class HotSeatV2 extends JPanel {

    GameMenuBar singleMenuBar;
    HotSeatBoard gameBoard;
    private LogDisplayText logDisplay;
    private LogDisplayContainer logContainer;
    TimerPanel timer;

    /**
     * Creates Jpanel for single player game
     *
     * @param jFrame parent JFrame to which it belongs
     * @param panel Jpanel from which was created/initialised (MainMenu)
     */
    public HotSeatV2(JFrame jFrame, JPanel panel) {
        Parameters param = new Parameters(null, null, null, null);

        try {
            gameBoard = new HotSeatBoard(param);
        } catch (AWTException | InterruptedException ex) {
            Logger.getLogger(HotSeatV2.class.getName()).log(Level.SEVERE, null, ex);
        }
        singleMenuBar = new GameMenuBar(jFrame, this, gameBoard.gameBoardLogic, true);

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 1;
        c.weightx = 1;
        c.weighty = 0;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.gridx = 0;
        c.gridy = 0;
        add(singleMenuBar, c);

        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 1;
        add(gameBoard, c);

        finalInit(jFrame, panel);
    }

    HotSeatV2(JFrame jFrame, JPanel panel, String lang, String flow, boolean flowB, int hrInt, int minInt, int secInt) {
        JPanel sidePanel = new JPanel();
        sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));

        logDisplay = new LogDisplayText();
        logContainer = new LogDisplayContainer(logDisplay);
        timer = new TimerPanel(lang, hrInt, minInt, secInt, flowB);
        Parameters param = new Parameters(lang, logDisplay, timer, flow);
        try {
            gameBoard = new HotSeatBoard(param);
        } catch (AWTException | InterruptedException ex) {
            Logger.getLogger(HotSeatV2.class.getName()).log(Level.SEVERE, null, ex);
        }
        singleMenuBar = new GameMenuBar(jFrame, this, gameBoard.gameBoardLogic, true);

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 0;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.gridx = 0;
        c.gridy = 0;
        add(singleMenuBar, c);

        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 1;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 1;
        add(gameBoard, c);

        sidePanel.add(timer);
        sidePanel.add(logContainer);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 0;
        c.weighty = 1;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 1;
        add(sidePanel, c);

        finalInit(jFrame, panel);
    }

    HotSeatV2(JFrame jFrame, JPanel panel, String flow, boolean flowB, int hrInt, int minInt, int secInt) {
        JPanel sidePanel = new JPanel();
        sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));

        timer = new TimerPanel("pl", hrInt, minInt, secInt, flowB);
        Parameters param = new Parameters(null, null, timer, flow);

        try {
            gameBoard = new HotSeatBoard(param);
        } catch (AWTException | InterruptedException ex) {
            Logger.getLogger(HotSeatV2.class.getName()).log(Level.SEVERE, null, ex);
        }
        singleMenuBar = new GameMenuBar(jFrame, this, gameBoard.gameBoardLogic, true);

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 0;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.gridx = 0;
        c.gridy = 0;
        add(singleMenuBar, c);

        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 1;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 1;
        add(gameBoard, c);

        sidePanel.add(timer);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 0;
        c.weighty = 1;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 1;
        add(sidePanel, c);

        finalInit(jFrame, panel);
    }

    HotSeatV2(JFrame jFrame, JPanel panel, String lang) {
        JPanel sidePanel = new JPanel();
        sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));

        logDisplay = new LogDisplayText();
        logContainer = new LogDisplayContainer(logDisplay);
        Parameters param = new Parameters(lang, logDisplay, null, null);
        try {
            gameBoard = new HotSeatBoard(param);
        } catch (AWTException | InterruptedException ex) {
            Logger.getLogger(HotSeatV2.class.getName()).log(Level.SEVERE, null, ex);
        }
        singleMenuBar = new GameMenuBar(jFrame, this, gameBoard.gameBoardLogic, true);

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 0;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.gridx = 0;
        c.gridy = 0;
        add(singleMenuBar, c);

        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 1;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 1;
        add(gameBoard, c);

        sidePanel.add(logContainer);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 0;
        c.weighty = 1;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 1;
        add(sidePanel, c);

        finalInit(jFrame, panel);
    }

    HotSeatV2(JFrame jFrame, JPanel panel, SaveFileData data) {
        Parameters param = new Parameters(null, null, null, null);
        JPanel sidePanel = new JPanel();
        boolean sidePanelFlag = false;
        if (data.isNotationFlag() || data.isTimerFlag()) {
            sidePanelFlag = true;
            sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));
        } else {
            param = new Parameters(null, null, null, null);
        }

        if(data.isNotationFlag()) {
            logDisplay = new LogDisplayText();
            logContainer = new LogDisplayContainer(logDisplay);
            logDisplay.setText(data.getLogText());            
        }
        
        if(data.isTimerFlag()){
            timer = new TimerPanel(data.getLanguage(),data.getWhitePlayerTime(),data.getBlackPlayerTime(), data.isTimeFlowBoolean());
            if(data.isNotationFlag()){
                param = new Parameters(data.getLanguage(), logDisplay, timer, data.getTimeFlow());
            }else{
                param = new Parameters(null, null, timer, data.getTimeFlow());
            }
        }
        
        
        try {
            gameBoard = new HotSeatBoard(param,data);
        } catch (AWTException | InterruptedException ex) {
            Logger.getLogger(HotSeatV2.class.getName()).log(Level.SEVERE, null, ex);
        }
        singleMenuBar = new GameMenuBar(jFrame, this, gameBoard.gameBoardLogic, true);

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        if (sidePanelFlag) {
            c.gridwidth = 2;
        } else {
            c.gridwidth = 1;
        }
        c.weightx = 1;
        c.weighty = 0;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.gridx = 0;
        c.gridy = 0;
        add(singleMenuBar, c);

        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 1;
        if (sidePanelFlag) {
            c.weighty = 0;
        } else {
            c.weighty = 1;
        }
        c.gridx = 0;
        c.gridy = 1;
        add(gameBoard, c);

        if (sidePanelFlag) {
            if(data.isTimerFlag()){
                sidePanel.add(timer);
            }
            if(data.isNotationFlag()){
                sidePanel.add(logContainer);
            }
            c.fill = GridBagConstraints.BOTH;
            c.anchor = GridBagConstraints.CENTER;
            c.weightx = 0;
            c.weighty = 1;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 1;
            add(sidePanel, c);
        }

        finalInit(jFrame, panel);
    }

    final void finalInit(JFrame jFrame, JPanel panel) {
        setEnabled(true);
        setVisible(true);
        jFrame.remove(panel);
        jFrame.add(this);
        jFrame.validate();
        jFrame.pack();
        jFrame.setLocationRelativeTo(null);
        jFrame.setMinimumSize(jFrame.getSize());
    }
}
