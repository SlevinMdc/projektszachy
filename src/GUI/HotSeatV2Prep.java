/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.NumberFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;
import javax.swing.text.BadLocationException;
import org.FileRead;
import org.SaveFileData;

/**
 *
 * @author TOM-12 <tomasz12@gmail.com>
 */
class HotSeatV2Prep extends JPanel implements ActionListener, FocusListener {

    private static final String ADD_TIMER = "Czy dodać zegar?", GIVE_TIME = "  Proszę podać czas(godz,minuty,sekundy):  ",
            FLOW_DOWN = "Odliczanie w dół", FLOW_UP = "Odliczanie w górę", ADD_NOTATION = "Czy dodać notację?",
            POLISH_NOTATION = "Polska notacja", ENGLISH_NOTATION = "Angielska notacja", RETURN_TO_MENU = "Powrót do menu",
            BEGIN_GAME = "Rozpocznij grę", LOAD_GAME = "Wczytaj grę", CREATE_NEW_GAME = "Utwórz nową grę";
    private JCheckBox timerChkB, notationChkB;
    private JPanel buttonsPanel, timerTFPanel, timerPanel, optionsPanel, loadPanel, choosingPanel;
    private JButton returnButton;
    private JButton acceptButton;
    private JButton loadButton;
    private JFormattedTextField hrTF, minTF, secTF;
    private JComboBox timeFlowCB, notationLangCB;
    private JLabel timerN;
    private JFrame jFrame;
    private boolean timer, notation, allow;
    private int hrInt, minInt, secInt;
    private String lang, flow;
    private JRadioButton loadGameRB, createGameRB;
    private SaveFileData data;

    public HotSeatV2Prep(JFrame jFrame, JPanel panel) {
        init(jFrame, panel);
    }

    private void init(final JFrame jFrame, JPanel panel) {
        this.jFrame = jFrame;

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        loadPanel = new JPanel();
        loadPanel.setLayout(new BoxLayout(loadPanel, BoxLayout.Y_AXIS));
        loadPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        loadButton = new JButton(LOAD_GAME);
        loadButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        loadButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                FileRead fileRead = new FileRead();
                data = fileRead.FileRead();
            }
        });
        loadPanel.add(Box.createHorizontalGlue());
        loadPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        loadPanel.add(loadButton);
        loadPanel.add(Box.createHorizontalGlue());
        loadPanel.add(Box.createRigidArea(new Dimension(0, 10)));

        optionsPanel = new JPanel();
        optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.Y_AXIS));
        optionsPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        timerChkB = new JCheckBox(ADD_TIMER);
        timerChkB.setSelected(false);
        timerChkB.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    timerPanel.setVisible(true);
                    jFrame.validate();
                    jFrame.revalidate();
                } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                    timerPanel.setVisible(false);
                    jFrame.validate();
                    jFrame.revalidate();
                }
            }
        });
        timerChkB.setAlignmentX(Component.CENTER_ALIGNMENT);
        optionsPanel.add(Box.createHorizontalGlue());
        optionsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        optionsPanel.add(timerChkB);
        optionsPanel.add(Box.createHorizontalGlue());

        timerPanel = new JPanel();
        timerPanel.setLayout(new BoxLayout(timerPanel, BoxLayout.Y_AXIS));
        timerPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        timerN = new JLabel(GIVE_TIME);
        timerN.setAlignmentX(Component.CENTER_ALIGNMENT);
        timerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        timerPanel.add(timerN);
        timerPanel.add(Box.createRigidArea(new Dimension(0, 5)));

        timerTFPanel = new JPanel();
        timerTFPanel.setLayout(new BoxLayout(timerTFPanel, BoxLayout.X_AXIS));

        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setParseIntegerOnly(true);
        formatter.setMaximumIntegerDigits(2);
        formatter.setMinimumIntegerDigits(1);

        hrTF = new JFormattedTextField(formatter);
        hrTF.setPreferredSize(new Dimension(20, 20));
        hrTF.setMaximumSize(new Dimension(20, 20));
        hrTF.addFocusListener(this);
        timerTFPanel.add(hrTF);
        timerTFPanel.add(Box.createRigidArea(new Dimension(5, 30)));
        timerTFPanel.add(new JLabel(":"));
        timerTFPanel.add(Box.createRigidArea(new Dimension(5, 30)));
        minTF = new JFormattedTextField(formatter);
        minTF.setPreferredSize(new Dimension(20, 20));
        minTF.setMaximumSize(new Dimension(20, 20));
        minTF.addFocusListener(this);
        timerTFPanel.add(minTF);
        timerTFPanel.add(Box.createRigidArea(new Dimension(5, 30)));
        timerTFPanel.add(new JLabel(":"));
        timerTFPanel.add(Box.createRigidArea(new Dimension(5, 30)));
        secTF = new JFormattedTextField(formatter);
        secTF.setPreferredSize(new Dimension(20, 20));
        secTF.setMaximumSize(new Dimension(20, 20));
        secTF.addFocusListener(this);
        timerTFPanel.add(secTF);
        timerPanel.add(timerTFPanel);

        DefaultListCellRenderer dlcr = new DefaultListCellRenderer();
        dlcr.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
        String[] timeFlowOpt = {FLOW_DOWN, FLOW_UP};
        timeFlowCB = new JComboBox(timeFlowOpt);
        timeFlowCB.setRenderer(dlcr);
        timeFlowCB.setPreferredSize(new Dimension(145, 20));
        timeFlowCB.setMaximumSize(new Dimension(145, 20));
        timerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        timerPanel.add(timeFlowCB);
        timerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        optionsPanel.add(timerPanel);

        notationChkB = new JCheckBox(ADD_NOTATION);
        notationChkB.setSelected(false);
        notationChkB.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getSource().equals(notationChkB)) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        notationLangCB.setVisible(true);
                        jFrame.validate();
                        jFrame.revalidate();
                    } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                        notationLangCB.setVisible(false);
                        jFrame.validate();
                        jFrame.revalidate();
                    }
                }
            }
        });
        notationChkB.setAlignmentX(Component.CENTER_ALIGNMENT);
        optionsPanel.add(notationChkB);
        optionsPanel.add(Box.createRigidArea(new Dimension(0, 5)));

        String[] notationLangOpt = {POLISH_NOTATION, ENGLISH_NOTATION};
        notationLangCB = new JComboBox(notationLangOpt);
        notationLangCB.setRenderer(dlcr);
        notationLangCB.setPreferredSize(new Dimension(145, 20));
        notationLangCB.setMaximumSize(new Dimension(145, 20));
        optionsPanel.add(notationLangCB);
        optionsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        optionsPanel.add(Box.createVerticalGlue());

        buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
        buttonsPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        buttonsPanel.add(Box.createHorizontalGlue());
        returnButton = new JButton(RETURN_TO_MENU);
        returnButton.addActionListener(this);
        buttonsPanel.add(returnButton);

        acceptButton = new JButton(BEGIN_GAME);
        acceptButton.addActionListener(this);
        buttonsPanel.add(acceptButton);
        buttonsPanel.add(Box.createHorizontalGlue());

        choosingPanel = new JPanel();
        choosingPanel.setLayout(new BoxLayout(choosingPanel, BoxLayout.Y_AXIS));
        choosingPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        loadGameRB = new JRadioButton(LOAD_GAME);
        loadGameRB.setSelected(false);
        loadGameRB.setAlignmentX(Component.CENTER_ALIGNMENT);
        choosingPanel.add(Box.createHorizontalGlue());
        choosingPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        choosingPanel.add(loadGameRB);
        choosingPanel.add(Box.createHorizontalGlue());
        choosingPanel.add(Box.createRigidArea(new Dimension(0, 10)));

        loadPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        choosingPanel.add(loadPanel);

        createGameRB = new JRadioButton(CREATE_NEW_GAME);
        createGameRB.setSelected(false);
        createGameRB.setAlignmentX(Component.CENTER_ALIGNMENT);
        choosingPanel.add(Box.createHorizontalGlue());
        choosingPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        choosingPanel.add(createGameRB);
        choosingPanel.add(Box.createHorizontalGlue());
        choosingPanel.add(Box.createRigidArea(new Dimension(0, 10)));

        ButtonGroup group = new ButtonGroup();
        group.add(loadGameRB);
        group.add(createGameRB);
        loadGameRB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                loadPanel.setVisible(true);
                optionsPanel.setVisible(false);

            }
        });
        createGameRB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                loadPanel.setVisible(false);
                optionsPanel.setVisible(true);
            }
        });

        optionsPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        choosingPanel.add(optionsPanel);
        add(choosingPanel);
        add(Box.createVerticalGlue());
        buttonsPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(buttonsPanel);

        setEnabled(true);
        setVisible(true);
        jFrame.remove(panel);
        jFrame.add(this);
        jFrame.validate();
        jFrame.pack();
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(290, jFrame.getHeight());
        jFrame.setMinimumSize(jFrame.getSize());
        timerPanel.setVisible(false);
        notationLangCB.setVisible(false);
        loadPanel.setVisible(false);
        optionsPanel.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equalsIgnoreCase(RETURN_TO_MENU)) {
            MainMenu mainMenu = new MainMenu(jFrame);
            jFrame.remove(this);
        }

        if (e.getActionCommand().equalsIgnoreCase(BEGIN_GAME)) {
            if (createGameRB.isSelected()) {
                timer = timerChkB.isSelected();
                notation = notationChkB.isSelected();
                allow = true;
                if (timer == true) {
                    if ("".equals(hrTF.getText())) {
                        hrInt = 0;
                        hrTF.setText("0");
                        allow = false;
                    } else {
                        hrInt = Integer.parseInt(hrTF.getText());
                    }
                    if ("".equals(minTF.getText())) {
                        minInt = 0;
                        minTF.setText("0");
                        allow = false;
                    } else {
                        minInt = Integer.parseInt(minTF.getText());
                    }
                    if ("".equals(secTF.getText())) {
                        secInt = 0;
                        secTF.setText("0");
                        allow = false;
                    } else {
                        secInt = Integer.parseInt(secTF.getText());
                    }

                    flow = (String) timeFlowCB.getSelectedItem();
                    if (flow.equals(FLOW_DOWN) && hrInt == 0 && minInt == 0 && secInt < 30) {
                        secTF.setText("30");
                        allow = false;
                    }
                }
                if (notation == true) {
                    lang = (String) notationLangCB.getSelectedItem();
                }
                if (allow == true) {
                    if (null != lang) {
                        switch (lang) {
                            case ENGLISH_NOTATION:
                                lang = "en";
                                break;
                            case POLISH_NOTATION:
                                lang = "pl";
                                break;
                        }
                    }
                    boolean flowB = false;
                    if (null != flow) {
                        switch (flow) {
                            case FLOW_UP:
                                flowB = true;
                                flow = "up";
                                break;
                            case FLOW_DOWN:
                                flowB = false;
                                flow = "down";
                                break;
                        }
                    }
                    if (notation == true && timer == false) {
                        HotSeatV2 game = new HotSeatV2(jFrame, this, lang);
                    }
                    if (notation == false && timer == true) {
                        HotSeatV2 game = new HotSeatV2(jFrame, this, flow, flowB, hrInt, minInt, secInt);
                    }
                    if (notation == true && timer == true) {
                        HotSeatV2 game = new HotSeatV2(jFrame, this, lang, flow, flowB, hrInt, minInt, secInt);
                    }
                    if (notation == false && timer == false) {
                        HotSeatV2 game = new HotSeatV2(jFrame, this);
                    }
                }
            }

            if (loadGameRB.isSelected() && data != null) {

                HotSeatV2 game = new HotSeatV2(jFrame, this, data);
            }
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        JFormattedTextField temp = (JFormattedTextField) e.getSource();
        if ("".equals(temp.getText())) {
            temp.setText("0");
        }
        if (temp.getText().substring(0, 1).equals("-")) {
            try {
                temp.setText(temp.getText(1, temp.getText().length()));
            } catch (BadLocationException ex) {
                Logger.getLogger(HotSeatV2Prep.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (!e.getSource().equals(hrTF)) {
            try {
                Integer.parseInt(temp.getText());
            } catch (NumberFormatException nFE) {
                temp.setText("");
            }
            if ("".equals(temp.getText())) {
            } else {
                try {
                    if (Integer.parseInt(temp.getText()) >= 60) {
                        temp.setText("59");
                    }
                } catch (NumberFormatException nFE) {
                }
            }
        }

    }

}
