/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author TOM-12 <tomasz12@gmail.com>
 */
public class LogDisplayContainer extends JPanel{

    /**
     * constructor for panel containing scroll pane containig text pane 
     * displaying moves iin checc notation
     * @param logDisplay 
     */
    public LogDisplayContainer(LogDisplayText logDisplay) {
        setLayout(new FlowLayout());
        JScrollPane pane =new JScrollPane();
        
        pane.setViewportView(logDisplay);
        pane.setPreferredSize(new Dimension(145,250)); 
        add(pane);
    }


}
