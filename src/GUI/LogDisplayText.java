/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

/**
 *
 * @author TOM-12 <tomasz12@gmail.com>
 */
public class LogDisplayText extends JTextPane{
    StyledDocument doc;    
    
    /**
     * Constructs text pane for displaying moves in chess notation
     */
    public LogDisplayText(){
        setEditable(false);
        setAutoscrolls(true);        
        doc = this.getStyledDocument();
    }
    
    /**
     * Adds text dfined by parametr str to log display
     * @param str   String to be added
     * @throws BadLocationException 
     */
    public void addText(String str) throws BadLocationException{
        if(doc.getLength()==0){
            doc.insertString(0, str, null );
        }
        else{
            doc.insertString(doc.getLength(), str, null);
        }
    }
    
    /**
     * Clears text pane
     */
    public void clear(){
        this.setText("");
    }
    
    
    @Override
    public void setText(String str){
        try {
            doc.insertString(0, str, null );
        } catch (BadLocationException ex) {
            Logger.getLogger(LogDisplayText.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
