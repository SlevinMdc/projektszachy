package GUI;


import java.awt.AWTException;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * 
 * @author TOM-12  <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class MainMenu extends JPanel implements ActionListener{    
    
    HotSeat hotSeat;
    HotSeatV2Prep hotSeatPrep;
    final JFrame frame;
    private static final String HOT_SEAT1="Hot seat v1",HOT_SEAT2="Hot seat v2",NOT_IMPLEMENTED="-",EXIT="Zamknij";
    String[] buttonArray = {HOT_SEAT1,HOT_SEAT2,NOT_IMPLEMENTED,NOT_IMPLEMENTED,EXIT};
    /**
     *  Constructs main menu panel
     * 
     * @param jFrame parent JFrame
     */
    public MainMenu(final JFrame jFrame){
        frame = jFrame;
        frame.setMinimumSize(new Dimension(200,(buttonArray.length*50)));
        frame.setSize(200,(buttonArray.length*50));
        frame.setLocationRelativeTo(null);
        
                
        setLayout(new BoxLayout(this , BoxLayout.Y_AXIS));
        
        
        
        for(int i=0;i<=buttonArray.length-1;++i){            
            createButton(buttonArray[i],i);
        }
        
        setEnabled(true);
        setVisible(true);
        final MainMenu aThis = this;
                        
        frame.add(aThis);
        frame.validate();
    }
    
    /**
     * Creates JButton
     * @param nazwa name of JButton as well as text
     * @param number number of JButton. If "0" adds vertical glue at top of the button
     */
    private void createButton(String nazwa,int number){
        if(number==0){
            add(Box.createVerticalGlue());
        }
        JButton button = new JButton(nazwa);
        button.setMaximumSize(new Dimension(100,25));
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.addActionListener(this);
        add(button);
        add(Box.createVerticalGlue());
    }

    @Override
    public void actionPerformed(ActionEvent e) {      
        String source = e.getActionCommand();
        
        switch (source) {
            case HOT_SEAT1:
                try {
                    hotSeat = new HotSeat(frame,this);
                } catch (AWTException | InterruptedException ex) {
                    Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
                }   break;
            case HOT_SEAT2:
                hotSeatPrep = new HotSeatV2Prep(frame,this);
                break;
            case NOT_IMPLEMENTED:
                throw new UnsupportedOperationException("przycisk \"-\" nie zaimplementowany");
            case EXIT:
                System.exit(0);
        }
    }
    
}
