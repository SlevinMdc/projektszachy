/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author TOM-12 <tomasz12@gmail.com>
 */
public class PromotionDialog extends JDialog implements MouseListener{

    String retVal;
    JPanel panel;
    
    /**
     * Creates window for choosing piece in promotion
     * @param color     color of promoting piece
     */
    public PromotionDialog(String color) {
        panel = new JPanel();
        setSize(new Dimension(240,60));        
        setLocationRelativeTo(null);
        String[] piecesArray = {color+"_queen",color+"_bishop",color+"_rook",color+"_knight"};
        
        for(int i=0;i<=piecesArray.length-1;++i){            
            createLabelButton(piecesArray[i],i);
        }
        
        panel.setForeground(Color.red);
        
        add(panel);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setUndecorated(true);
    }

    /**
     * Shows dialog
     * @return String containig name of chosen piece
     */
    public String showDialog(){
        setVisible(true);
        return retVal;
    }

    private void createLabelButton(String string, int i) {
        if(i==0){
            add(Box.createVerticalGlue());
        }
        JLabel label = new JLabel(string);
        label.setMaximumSize(new Dimension(50,50));
        label.setPreferredSize(new Dimension(50,50));
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setText(string);
        label.addMouseListener(this);
        label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/"+string+".png")));
        label.setBorder(BorderFactory.createRaisedSoftBevelBorder());
        panel.add(label);
        //label.setVisible(true);
        panel.add(Box.createVerticalGlue());
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        JLabel l;
        l=(JLabel) e.getSource();        
        retVal=l.getText();
        setVisible(false);
        dispose();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    
}
