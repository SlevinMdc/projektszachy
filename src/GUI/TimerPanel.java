/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author TOM-12 <tomasz12@gmail.com>
 */
public final class TimerPanel extends JPanel {

    private Timer blackTimex;
    private Timer whiteTimex;
    private String startTime;
    private String startTimeBlack;
    private String startTimeWhite;

    private final JLabel whiteLabel = new JLabel();
    private final JLabel blackLabel = new JLabel();

    private final JLabel whiteTimeDisplay = new JLabel();
    private final JLabel blackTimeDisplay = new JLabel();

    private final JPanel whitePanel = new JPanel();
    private final JPanel blackPanel = new JPanel();

    private String stimeHour;
    private String stimeMinute;
    private String stimeSecond;

    private boolean upTime;

    ActionListener blackTaskPerformer = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (upTime == true) {
                upTime(blackTimeDisplay);
            } else {
                if (upTime == false) {
                    downTime(blackTimeDisplay);
                }
            }
            blackTimex.restart();
        }
    };
    ActionListener whiteTaskPerformer = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (upTime == true) {
                upTime(whiteTimeDisplay);
            } else {
                if (upTime == false) {
                    downTime(whiteTimeDisplay);
                }
            }
            whiteTimex.restart();
        }
    };

    /**
     * Creates timer panel containing timers for indicating how much time left
     * to end game or from start
     *
     * @param language in what language display "en" or "pl"
     * @param th hours
     * @param tm minutes
     * @param ts second
     * @param upTime true if time going up or false if time going down
     */
    public TimerPanel(String language, int th, int tm, int ts, boolean upTime) {
        this.upTime = upTime;
        convertTime(th, tm, ts);
        init(language);
        resetTime();

        whiteTimeDisplay.setAlignmentX(Component.CENTER_ALIGNMENT);
        blackTimeDisplay.setAlignmentX(Component.CENTER_ALIGNMENT);
        whitePanel.add(whiteTimeDisplay);
        blackPanel.add(blackTimeDisplay);

        add(whitePanel);
        add(Box.createRigidArea(new Dimension(5, 0)));
        add(blackPanel);
    }

    TimerPanel(String language, int whitePlayerTime, int blackPlayerTime, boolean upTime) {
        this.upTime = upTime;
        init(language);

        setTimers(whitePlayerTime, blackPlayerTime);

        whiteTimeDisplay.setAlignmentX(Component.CENTER_ALIGNMENT);
        blackTimeDisplay.setAlignmentX(Component.CENTER_ALIGNMENT);
        whitePanel.add(whiteTimeDisplay);
        blackPanel.add(blackTimeDisplay);
        add(whitePanel);
        add(Box.createRigidArea(new Dimension(5, 0)));
        add(blackPanel);
    }

    private void init(String language) {
        setPreferredSize(new Dimension(135, 150));

        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        whitePanel.setLayout(new BoxLayout(whitePanel, BoxLayout.PAGE_AXIS));
        blackPanel.setLayout(new BoxLayout(blackPanel, BoxLayout.PAGE_AXIS));

        if (language.equalsIgnoreCase("pl")) {
            whiteLabel.setText("Białe");
            blackLabel.setText("Czarne");
        }
        if (language.equalsIgnoreCase("en")) {
            whiteLabel.setText("White");
            blackLabel.setText("Black");
        }

        whiteLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        blackLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        whitePanel.add(whiteLabel);
        blackPanel.add(blackLabel);

        blackTimex = new Timer(1_000, blackTaskPerformer);
        whiteTimex = new Timer(1_000, whiteTaskPerformer);
    }

    /**
     * Starts timer for black player
     */
    public void blackStart() {
        blackTimex.start();
    }

    /**
     * Stops timer for black player
     */
    public void blackStop() {
        blackTimex.stop();
    }

    /**
     * Starts timer for white player
     */
    public void whiteStart() {
        whiteTimex.start();
    }

    /**
     * Stops timer for white player
     */
    public void whiteStop() {
        whiteTimex.stop();
    }

    public void pause() {
        whiteTimex.stop();
        blackTimex.stop();
    }

    /**
     * Stops timers for black and white player
     */
    public void stop() {
        whiteTimex.removeActionListener(whiteTaskPerformer);
        blackTimex.removeActionListener(blackTaskPerformer);
    }

    /**
     * displays time going up
     *
     * @param timer JLabel on which display
     */
    private void upTime(JLabel timer) {
        String text = timer.getText();
        int s0 = Integer.parseInt(text.substring(11, 12));
        int s1 = Integer.parseInt(text.substring(10, 11));
        int m0 = Integer.parseInt(text.substring(6, 7));
        int m1 = Integer.parseInt(text.substring(5, 6));
        int h0 = Integer.parseInt(text.substring(1, 2));
        int h1 = Integer.parseInt(text.substring(0, 1));

        if (s0 == 9) {
            s0 = 0;
            ++s1;
        } else if (s0 != 9) {
            ++s0;
        }
        if (s1 == 6) {
            s0 = 0;
            s1 = 0;
            ++m0;
        }
        if (m0 == 10) {
            m0 = 0;
            ++m1;
        }
        if (m1 == 6) {
            m1 = 0;
            ++h0;
        }
        if (h0 == 10) {
            h0 = 0;
            ++h1;
        }
        if (h1 == 10) {
            s0 = 0;
            s1 = 0;
            m0 = 0;
            m1 = 0;
            h0 = 0;
            h1 = 0;
        }

        String s_s0 = Integer.toString(s0);
        String s_s1 = Integer.toString(s1);
        String s_m0 = Integer.toString(m0);
        String s_m1 = Integer.toString(m1);
        String s_h0 = Integer.toString(h0);
        String s_h1 = Integer.toString(h1);
        timer.setText(s_h1 + s_h0 + " : " + s_m1 + s_m0 + " : " + s_s1 + s_s0);

    }

    /**
     * displays time going down
     *
     * @param timer JLabel on which display
     */
    private void downTime(JLabel timer) {
        String text = timer.getText();
        int s0 = Integer.parseInt(text.substring(11, 12));
        int s1 = Integer.parseInt(text.substring(10, 11));
        int m0 = Integer.parseInt(text.substring(6, 7));
        int m1 = Integer.parseInt(text.substring(5, 6));
        int h0 = Integer.parseInt(text.substring(1, 2));
        int h1 = Integer.parseInt(text.substring(0, 1));

        --s0;
        if (s0 < 0) {
            s0 = 9;
            --s1;

        }
        if (s1 < 0) {
            s1 = 5;
            --m0;
        }
        if (m0 < 0) {
            m0 = 9;
            --m1;
        }
        if (m1 < 0) {
            m1 = 5;
            --h0;
        }
        if (h0 < 0) {
            h0 = 9;
            --h1;
        }
        if (h1 < 0) {
            h1 = 9;
        }

        String s_s0 = Integer.toString(s0);
        String s_s1 = Integer.toString(s1);
        String s_m0 = Integer.toString(m0);
        String s_m1 = Integer.toString(m1);
        String s_h0 = Integer.toString(h0);
        String s_h1 = Integer.toString(h1);
        timer.setText(s_h1 + s_h0 + " : " + s_m1 + s_m0 + " : " + s_s1 + s_s0);
    }

    /**
     * converts time from constructor and displays it on labels
     *
     * @param th hours
     * @param tm minutes
     * @param ts seconds
     */
    private void convertTime(int th, int tm, int ts) {
        if (th < 10) {
            stimeHour = "0".concat(Integer.toString(th));
        } else {
            stimeHour = Integer.toString(th);
        }
        if (tm < 10) {
            stimeMinute = "0".concat(Integer.toString(tm));
        } else {
            stimeMinute = Integer.toString(tm);
        }
        if (ts < 10) {
            stimeSecond = "0".concat(Integer.toString(ts));
        } else {
            stimeSecond = Integer.toString(ts);
        }
        startTime = stimeHour.concat(" : " + stimeMinute.concat(" : " + stimeSecond));
        startTimeWhite=startTime;
        startTimeBlack=startTime;
    }

    /**
     * Gets white player time
     *
     * @return white players time
     */
    public int getWhitePlayerTime() {
        String text = whiteTimeDisplay.getText();
        int s0 = Integer.parseInt(text.substring(11, 12));
        int s1 = Integer.parseInt(text.substring(10, 11));
        int m0 = Integer.parseInt(text.substring(6, 7));
        int m1 = Integer.parseInt(text.substring(5, 6));
        int h0 = Integer.parseInt(text.substring(1, 2));
        int h1 = Integer.parseInt(text.substring(0, 1));
        return (h1 * 100_000) + (h0 * 10_000) + (m1 * 1_000) + (m0 * 100) + (s1 * 10) + (s0);
    }

    /**
     * Gets black player time
     *
     * @return black players time
     */
    public int getBlackPlayerTime() {
        String text = blackTimeDisplay.getText();
        int s0 = Integer.parseInt(text.substring(11, 12));
        int s1 = Integer.parseInt(text.substring(10, 11));
        int m0 = Integer.parseInt(text.substring(6, 7));
        int m1 = Integer.parseInt(text.substring(5, 6));
        int h0 = Integer.parseInt(text.substring(1, 2));
        int h1 = Integer.parseInt(text.substring(0, 1));
        return (h1 * 100_000) + (h0 * 10_000) + (m1 * 1_000) + (m0 * 100) + (s1 * 10) + (s0);
    }

    /**
     * Resets and stops both players timers and displays
     */
    public void resetTime() {
        whiteTimeDisplay.setText(startTimeWhite);
        blackTimeDisplay.setText(startTimeBlack);
        if (blackTimex.getActionListeners().length == 0) {
            blackTimex.addActionListener(blackTaskPerformer);
            whiteTimex.addActionListener(whiteTaskPerformer);
        }
        blackTimex.stop();
        whiteTimex.stop();
    }

    private void setTimers(int whitePlayerTime, int blackPlayerTime) {
        startTimeWhite=getConvertedTime(whitePlayerTime);
        startTimeBlack=getConvertedTime(blackPlayerTime);
        whiteTimeDisplay.setText(startTimeWhite);
        blackTimeDisplay.setText(startTimeBlack);
        if (blackTimex.getActionListeners().length == 0) {
            blackTimex.addActionListener(blackTaskPerformer);
            whiteTimex.addActionListener(whiteTaskPerformer);
        }
        blackTimex.stop();
        whiteTimex.stop();
    }

    private String getConvertedTime(int time) {
        String timeS;
        int th, tm, ts;
        String hourS, minuteS, secondS;
        th = time / 10_000;
        
        tm = time - (th * 10_000);
        tm = tm / 100;
        
        ts = time - (th * 10_000) - (tm * 100);

        if (th < 10) {
            hourS = "0".concat(Integer.toString(th));
        } else {
            hourS = Integer.toString(th);
        }
        if (tm < 10) {
            minuteS = "0".concat(Integer.toString(tm));
        } else {
            minuteS = Integer.toString(tm);
        }
        if (ts < 10) {
            secondS = "0".concat(Integer.toString(ts));
        } else {
            secondS = Integer.toString(ts);
        }

        timeS = hourS.concat(" : " + minuteS.concat(" : " + secondS));

        return timeS;
    }
}
