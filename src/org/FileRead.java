/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author TOM-12 <tomasz12@gmail.com>
 */
public class FileRead {

    public FileRead() {
    }

    public SaveFileData FileRead() {

        File file = ChooseFile();
        if(file==null){
            return null;
        }
        SaveFileData data = null;
        try {

            FileInputStream fout = new FileInputStream(file);
            try (ObjectInputStream ois = new ObjectInputStream(fout)) {
                try {
                    data=(SaveFileData) ois.readObject();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(FileRead.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (IOException ex) {
        }
        
        return data;
    }

    private File ChooseFile() {
        File file = null;
        boolean acceptable = false;

        JFileChooser fileChooser = new JFileChooser();
        //fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Save game files", "save"));
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileFilter(new FileNameExtensionFilter("Save game files", "save"));
        do {
            File f;
            if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                f = fileChooser.getSelectedFile();
                if (f.exists() && nameValid(f.getName())) {
                    acceptable = true;
                    file=f;
                }

            } else {
                acceptable = true;
            }
        } while (!acceptable);

        return file;
    }

    private boolean nameValid(String filename) {
        boolean flag = false;

        
        int lastdot = filename.lastIndexOf(".");
        if (lastdot != -1) {
            if (filename.substring(lastdot+1, filename.length()).equalsIgnoreCase("save")) {
                flag = true;
            }
        } else {
            flag = false;
        }
        return flag;
    }

}
