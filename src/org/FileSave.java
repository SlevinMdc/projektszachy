/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author TOM-12 <tomasz12@gmail.com>
 */
public class FileSave {

    public FileSave(JFrame frame, GameBoardLogic gameBoard) {

        if (gameBoard.timer != null) {
            gameBoard.timer.pause();
        }
        File file = ChooseFile();
        if (file == null) {
            if (gameBoard.timer != null) {
                if (gameBoard.turns % 2 == 1) {
                    gameBoard.timer.whiteStart();
                } else {
                    gameBoard.timer.blackStart();
                }
            }
            return;
        }

        SaveFileData data;
        data = gameBoard.getSaveData();
        SaveFile(file, data);

        /*
         System.out.println(data.isTimingFlag());
         System.out.println(data.getTimeFlow());
         System.out.println(data.isNotationFlag());
         System.out.println(data.getLanguage());
         System.out.println(data.getText());
         System.out.println(data.getTurn());
         System.out.println(data.getTurns());
         System.out.println(data.getBlackPlayerTime());
         System.out.println(data.getWhitePlayerTime());
         System.out.println(data);*/
        if (gameBoard.timer != null) {
            if (gameBoard.turns % 2 == 1) {
                gameBoard.timer.whiteStart();
            } else {
                gameBoard.timer.blackStart();
            }
        }
    }

    private File ChooseFile() {
        File file = null;
        boolean acceptable = false;

        do {
            File f;
            JFileChooser fileChooser = new JFileChooser();
            if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {

                f = fileChooser.getSelectedFile();
                String filename = f.getName();

                filename = editName(filename);
                f = new File(f.getAbsolutePath().replace(f.getName(), "") + filename);

                if (f.exists()) {
                    int result = JOptionPane.showConfirmDialog(fileChooser,"Plik istnieje, nadpisać?","Plik istnieje",JOptionPane.YES_NO_CANCEL_OPTION);
                    if (result == JOptionPane.YES_OPTION) {
                        acceptable = true;
                        file = f;
                    }
                } else {
                    acceptable = true;
                    file = f;
                }
            } else {
                acceptable = true;
            }
        } while (!acceptable);

        return file;
    }

    private String editName(String filename) {
        String finalName;
        int lastdot;
        lastdot = filename.lastIndexOf(".");

        if (lastdot != -1) {
            if (!filename.substring(lastdot, filename.length()).equalsIgnoreCase("save")) {
                filename = filename.substring(0, lastdot).concat(".save");
            }
        } else {
            filename = filename.concat(".save");
        }
        finalName = filename;
        return finalName;
    }

    private void SaveFile(File file, SaveFileData data) {

        try {

            FileOutputStream fout = new FileOutputStream(file);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(data);
            }
        } catch (IOException ex) {
        }

    }

}
