package org;

import GUI.HotSeatBoard;
import GUI.LogDisplayText;
import GUI.PromotionDialog;
import GUI.TimerPanel;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.text.BadLocationException;
import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;
import pieces.XY;

/**
 *
 * @author TOM-12 <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class GameBoardLogic extends JPanel{
       
    private String cell[][] = new String[8][8];
    private Piece pieces[][] = new Piece[8][8];
    private JLabel chessPiece;  
    private Container originalContainer;
    
    ;
    private ArrayList<XY> moveList = new ArrayList<>();
    private final ArrayList<XY> availableSquares = new ArrayList<>();

    private LogDisplayText logDisplay;
    private HotSeatBoard gameBoard;
    private boolean notation=false,timing=false;
    private int turn;
    int turns;
    private String language="en";
    TimerPanel timer;
    String timeFlow;
    
    
    private final ActionListener taskPerformer = new ActionListener(){

        @Override
        public void actionPerformed(ActionEvent e) {
            if( (turns%2)==1 ){
                if(timer.getWhitePlayerTime()==0){
                    gameBoard.endGame=true;
                    String s;
                    s = "\n";
                    s = s.concat("0-1");
                    updateLogDisplay(s + "\n");
                    timer.stop();
                }
            }
            if( (turns%2)==0 ){
                if(timer.getBlackPlayerTime()==0){
                    gameBoard.endGame=true;
                    String s;
                    s = "\n";
                    s = s.concat("1-0");
                    updateLogDisplay(s + "\n");
                    timer.stop();
                }
            }            
        }
    };
    

    /**
     * Creates Board for single player game
     *
     * @param para
     * @throws java.awt.AWTException
     * @throws java.lang.InterruptedException
     */ 
    public GameBoardLogic(Parameters para) throws AWTException, InterruptedException{
         if(para.isTimer()){
             timer=para.getTimer();
             timing=true;
         }
         if(para.isLogDisplay()){
             logDisplay = para.getLogDisplay();
             notation=true;
         }
         if(para.isLang()){
             language=para.getLang();
         }
         if(para.isTimer()){
             timer=para.getTimer();
             if(para.isTimeFlow()){
                 if(para.getTimeFlow().equalsIgnoreCase("up")){
                     timeFlow="up";
                 }
                 if(para.getTimeFlow().equalsIgnoreCase("down")){
                     Timer timer1 = new Timer(100, taskPerformer);
                     timer1.start();
                     timeFlow="down";
                 }
             }
         }
         
         
         if(para.getBoardClassName().equalsIgnoreCase("HotSeatBoard")){
             gameBoard=(HotSeatBoard) para.getBoard();
         }
         
     }

    /**
     * move piece on board. returns true if move was executed(legal) or false if not
     * @param chessPiec chess Piece to move
     * @param layeredPane   
     * @param originalConta original square of chess piece
     * @param destinationContainer  destination square of chess piece
     * @return true if move was executed 
     */
    public boolean makeMove(JLabel chessPiec, JLayeredPane layeredPane, Container originalConta, Container destinationContainer) {
        boolean correctMove=false;
        this.chessPiece=chessPiec;
        this.originalContainer=originalConta;        
        int sourceRow = getXLocation(originalContainer.getLocation());
        int sourceCol =  getYLocation(originalContainer.getLocation());
        int destRow = getXLocation(destinationContainer.getLocation());
        int destCol = getYLocation(destinationContainer.getLocation());
        chessPiece.setVisible(false);
        layeredPane.remove(chessPiece);
        chessPiece.setVisible(true);

        if (gameBoard.square[destRow][destCol].getComponentCount() != 0) {//if drop destination is not empty
            for (int i = 0; i <= availableSquares.size() - 1; ++i) {//checking if drop destination is on availableSquares list - is it legal move
                if (gameBoard.square[availableSquares.get(i).getX()][availableSquares.get(i).getY()].getName().equalsIgnoreCase(gameBoard.square[destRow][destCol].getName())) {
                    gameBoard.square[destRow][destCol].remove(0);
                    gameBoard.square[destRow][destCol].add(chessPiece);
                    gameBoard.square[destRow][destCol].validate();
                    if (chessPiece.getClass().toString().toLowerCase().contains("pawn".toLowerCase())
                            && ((((Piece) chessPiece).getPieceColor().equalsIgnoreCase("b") && destRow == 7) || (((Piece) chessPiece).getPieceColor().equalsIgnoreCase("w") && destRow == 0))) {
                        PromotionDialog dlg = new PromotionDialog(((Piece) chessPiece).getPieceColor());
                        timer.pause();
                        String value = dlg.showDialog();
                        Piece piece;
                        switch (value) {
                            case "b_queen":
                                piece = new Queen("b");
                                gameBoard.square[destRow][destCol].remove(0);
                                chessPiece = (JLabel) piece;
                                gameBoard.square[destRow][destCol].add(chessPiece);
                                gameBoard.square[destRow][destCol].validate();
                                cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                pieces[sourceRow][sourceCol] = piece;
                                timer.blackStart();
                                break;
                            case "w_queen":
                                piece = new Queen("w");
                                gameBoard.square[destRow][destCol].remove(0);
                                chessPiece = (JLabel) piece;
                                gameBoard.square[destRow][destCol].add(chessPiece);
                                gameBoard.square[destRow][destCol].validate();
                                cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                pieces[sourceRow][sourceCol] = piece;
                                timer.whiteStart();
                                break;
                            case "b_rook":
                                piece = new Rook("b");
                                gameBoard.square[destRow][destCol].remove(0);
                                chessPiece = (JLabel) piece;
                                gameBoard.square[destRow][destCol].add(chessPiece);
                                gameBoard.square[destRow][destCol].validate();
                                cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                pieces[sourceRow][sourceCol] = piece;
                                timer.blackStart();
                                break;
                            case "w_rook":
                                piece = new Rook("w");
                                gameBoard.square[destRow][destCol].remove(0);
                                chessPiece = (JLabel) piece;
                                gameBoard.square[destRow][destCol].add(chessPiece);
                                gameBoard.square[destRow][destCol].validate();
                                cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                pieces[sourceRow][sourceCol] = piece;
                                timer.whiteStart();
                                break;
                            case "b_bishop":
                                piece = new Bishop("b");
                                gameBoard.square[destRow][destCol].remove(0);
                                chessPiece = (JLabel) piece;
                                gameBoard.square[destRow][destCol].add(chessPiece);
                                gameBoard.square[destRow][destCol].validate();
                                cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                pieces[sourceRow][sourceCol] = piece;
                                timer.blackStart();
                                break;
                            case "w_bishop":
                                piece = new Bishop("w");
                                gameBoard.square[destRow][destCol].remove(0);
                                chessPiece = (JLabel) piece;
                                gameBoard.square[destRow][destCol].add(chessPiece);
                                gameBoard.square[destRow][destCol].validate();
                                cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                pieces[sourceRow][sourceCol] = piece;
                                timer.whiteStart();
                                break;
                            case "b_knight":
                                piece = new Knight("b");
                                gameBoard.square[destRow][destCol].remove(0);
                                chessPiece = (JLabel) piece;
                                gameBoard.square[destRow][destCol].add(chessPiece);
                                gameBoard.square[destRow][destCol].validate();
                                cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                pieces[sourceRow][sourceCol] = piece;
                                timer.blackStart();
                                break;
                            case "w_knight":
                                piece = new Knight("w");
                                gameBoard.square[destRow][destCol].remove(0);
                                chessPiece = (JLabel) piece;
                                gameBoard.square[destRow][destCol].add(chessPiece);
                                gameBoard.square[destRow][destCol].validate();
                                cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                pieces[sourceRow][sourceCol] = piece;
                                timer.whiteStart();
                                break;
                        }
                        updateArrays(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY(), 4);
                        finishMove();
                        correctMove=true;
                    } else {
                        updateArrays(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY(), 1);
                        finishMove();
                        correctMove=true;
                        break;
                    }
                } else {//if move is illegal drop piece back to its original gameBoard.square
                    if (!gameBoard.square[availableSquares.get(i).getX()][availableSquares.get(i).getY()].getName().equalsIgnoreCase(gameBoard.square[destRow][destCol].getName())) {
                        rejectedMove();
                        correctMove=false;
                    }
                }
            }
            if (availableSquares.isEmpty()) {//if list of available gameBoard.squares for this piece is empty - no move for this piece - drop piece back to its original gameBoard.squares
                rejectedMove();
                correctMove=false;
            }
        } else {//drop destination is empty gameBoard.square            
            for (int i = 0; i <= availableSquares.size() - 1; ++i) {//iterating through available moves for this piece
                if (gameBoard.square[availableSquares.get(i).getX()][availableSquares.get(i).getY()].getName().equalsIgnoreCase(gameBoard.square[destRow][destCol].getName())) {
                    gameBoard.square[destRow][destCol].add(chessPiece);
                    gameBoard.square[destRow][destCol].validate();
                    
                    if (chessPiece.getClass().toString().toLowerCase().contains("pawn".toLowerCase())) {//if picked up piece is pawn                    
                        int val = Math.abs(getXLocation(originalContainer.getLocation()) - availableSquares.get(i).getX());
                        if (val == 2) {//if pawn moved 2 gameBoard.squares in one move
                            ((Pawn) chessPiece).twoSquareMoved();
                            updateArrays(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY(), 0);
                            finishMove();
                            correctMove=true;
                        } else {//checking & executing en passant move
                            if (checkEnPassant(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY())) {
                                if (((Piece) chessPiece).getPieceColor().equalsIgnoreCase("b")) {
                                    gameBoard.square[4][availableSquares.get(i).getY()].remove(0);
                                    gameBoard.square[4][availableSquares.get(i).getY()].updateUI();
                                    gameBoard.square[4][availableSquares.get(i).getY()].validate();
                                    updateArrays(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY(), 2);
                                    finishMove();
                                    correctMove=true;
                                }
                                if (((Piece) chessPiece).getPieceColor().equalsIgnoreCase("w")) {
                                    gameBoard.square[3][availableSquares.get(i).getY()].remove(0);
                                    gameBoard.square[3][availableSquares.get(i).getY()].updateUI();
                                    gameBoard.square[3][availableSquares.get(i).getY()].validate();
                                    updateArrays(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY(), 2);
                                    finishMove();
                                    correctMove=true;
                                }
                            } else if ((((Piece) chessPiece).getPieceColor().equalsIgnoreCase("b") && destRow == 7) || (((Piece) chessPiece).getPieceColor().equalsIgnoreCase("w") && destRow == 0)) {
                                PromotionDialog dlg = new PromotionDialog(((Piece) chessPiece).getPieceColor());
                                timer.pause();
                                String value = dlg.showDialog();
                                Piece piece;
                                switch (value) {
                                    case "b_queen":
                                        piece = new Queen("b");
                                        gameBoard.square[destRow][destCol].remove(0);
                                        chessPiece = (JLabel) piece;
                                        gameBoard.square[destRow][destCol].add(chessPiece);
                                        gameBoard.square[destRow][destCol].validate();
                                        cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                        pieces[sourceRow][sourceCol] = piece;
                                        timer.blackStart();
                                        break;
                                    case "w_queen":
                                        piece = new Queen("w");
                                        gameBoard.square[destRow][destCol].remove(0);
                                        chessPiece = (JLabel) piece;
                                        gameBoard.square[destRow][destCol].add(chessPiece);
                                        gameBoard.square[destRow][destCol].validate();
                                        cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                        pieces[sourceRow][sourceCol] = piece;
                                        timer.whiteStart();
                                        break;
                                    case "b_rook":
                                        piece = new Rook("b");
                                        gameBoard.square[destRow][destCol].remove(0);
                                        chessPiece = (JLabel) piece;
                                        gameBoard.square[destRow][destCol].add(chessPiece);
                                        gameBoard.square[destRow][destCol].validate();
                                        cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                        pieces[sourceRow][sourceCol] = piece;
                                        timer.blackStart();
                                        break;
                                    case "w_rook":
                                        piece = new Rook("w");
                                        gameBoard.square[destRow][destCol].remove(0);
                                        chessPiece = (JLabel) piece;
                                        gameBoard.square[destRow][destCol].add(chessPiece);
                                        gameBoard.square[destRow][destCol].validate();
                                        cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                        pieces[sourceRow][sourceCol] = piece;
                                        timer.whiteStart();
                                        break;
                                    case "b_bishop":
                                        piece = new Bishop("b");
                                        gameBoard.square[destRow][destCol].remove(0);
                                        chessPiece = (JLabel) piece;
                                        gameBoard.square[destRow][destCol].add(chessPiece);
                                        gameBoard.square[destRow][destCol].validate();
                                        cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                        pieces[sourceRow][sourceCol] = piece;
                                        timer.blackStart();
                                        break;
                                    case "w_bishop":
                                        piece = new Bishop("w");
                                        gameBoard.square[destRow][destCol].remove(0);
                                        chessPiece = (JLabel) piece;
                                        gameBoard.square[destRow][destCol].add(chessPiece);
                                        gameBoard.square[destRow][destCol].validate();
                                        cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                        pieces[sourceRow][sourceCol] = piece;
                                        timer.whiteStart();
                                        break;
                                    case "b_knight":
                                        piece = new Knight("b");
                                        gameBoard.square[destRow][destCol].remove(0);
                                        chessPiece = (JLabel) piece;
                                        gameBoard.square[destRow][destCol].add(chessPiece);
                                        gameBoard.square[destRow][destCol].validate();
                                        cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                        pieces[sourceRow][sourceCol] = piece;
                                        timer.blackStart();
                                        break;
                                    case "w_knight":
                                        piece = new Knight("w");
                                        gameBoard.square[destRow][destCol].remove(0);
                                        chessPiece = (JLabel) piece;
                                        gameBoard.square[destRow][destCol].add(chessPiece);
                                        gameBoard.square[destRow][destCol].validate();
                                        cell[sourceRow][sourceCol] = piece.getPieceFullName();
                                        pieces[sourceRow][sourceCol] = piece;
                                        timer.whiteStart();
                                        break;
                                }
                                updateArrays(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY(), 3);
                                finishMove();
                                correctMove=true;
                            } else {
                                updateArrays(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY(), 0);
                                finishMove();
                                correctMove=true;
                            }
                        }
                    } else if (chessPiece.getClass().toString().toLowerCase().contains("king".toLowerCase())) {//if picked up piece is king
                        int val = Math.abs(getYLocation(originalContainer.getLocation()) - availableSquares.get(i).getY());
                        if (val == 2) {
                            Piece piece;
                            if (destRow == 7) {
                                if (destCol == 2) {
                                    piece = pieces[7][0];
                                    gameBoard.square[7][0].remove(0);
                                    gameBoard.square[7][0].updateUI();
                                    gameBoard.square[7][0].validate();
                                    JLabel p = (JLabel) piece;
                                    gameBoard.square[7][3].add(p);
                                    gameBoard.square[7][0].validate();
                                    pieces[destRow][destCol] = pieces[sourceRow][sourceCol];
                                    pieces[sourceRow][sourceCol] = null;
                                    cell[destRow][destCol] = cell[sourceRow][sourceCol];
                                    cell[sourceRow][sourceCol] = null;

                                    pieces[7][3] = pieces[7][0];
                                    pieces[7][0] = null;
                                    cell[7][3] = cell[7][0];
                                    cell[7][0] = null;

                                    updateArrays(0, 0, 0, 0, 6);
                                    finishMove();
                                    correctMove=true;
                                }
                                if (destCol == 6) {
                                    piece = pieces[7][7];
                                    gameBoard.square[7][7].remove(0);
                                    gameBoard.square[7][7].updateUI();
                                    gameBoard.square[7][7].validate();
                                    JLabel p = (JLabel) piece;
                                    gameBoard.square[7][5].add(p);
                                    gameBoard.square[7][7].validate();
                                    pieces[destRow][destCol] = pieces[sourceRow][sourceCol];
                                    pieces[sourceRow][sourceCol] = null;
                                    cell[destRow][destCol] = cell[sourceRow][sourceCol];
                                    cell[sourceRow][sourceCol] = null;

                                    pieces[7][5] = pieces[7][7];
                                    pieces[7][7] = null;
                                    cell[7][5] = cell[7][7];
                                    cell[7][7] = null;

                                    updateArrays(0, 0, 0, 0, 5);
                                    finishMove();
                                    correctMove=true;
                                }
                            }
                            if (destRow == 0) {
                                if (destCol == 2) {
                                    piece = pieces[0][0];
                                    gameBoard.square[0][0].remove(0);
                                    gameBoard.square[0][0].updateUI();
                                    gameBoard.square[0][0].validate();
                                    JLabel p = (JLabel) piece;
                                    gameBoard.square[0][3].add(p);
                                    gameBoard.square[0][0].validate();
                                    pieces[destRow][destCol] = pieces[sourceRow][sourceCol];
                                    pieces[sourceRow][sourceCol] = null;
                                    cell[destRow][destCol] = cell[sourceRow][sourceCol];
                                    cell[sourceRow][sourceCol] = null;

                                    pieces[0][3] = pieces[0][0];
                                    pieces[0][0] = null;
                                    cell[0][3] = cell[0][0];
                                    cell[0][0] = null;

                                    updateArrays(0, 0, 0, 0, 6);
                                    finishMove();
                                    correctMove=true;
                                }
                                if (destCol == 6) {
                                    piece = pieces[0][7];
                                    gameBoard.square[0][7].remove(0);
                                    gameBoard.square[0][7].updateUI();
                                    gameBoard.square[0][7].validate();
                                    JLabel p = (JLabel) piece;
                                    gameBoard.square[0][5].add(p);
                                    gameBoard.square[0][7].validate();
                                    pieces[destRow][destCol] = pieces[sourceRow][sourceCol];
                                    pieces[sourceRow][sourceCol] = null;
                                    cell[destRow][destCol] = cell[sourceRow][sourceCol];
                                    cell[sourceRow][sourceCol] = null;

                                    pieces[0][5] = pieces[0][7];
                                    pieces[0][7] = null;
                                    cell[0][5] = cell[0][7];
                                    cell[0][7] = null;

                                    updateArrays(0, 0, 0, 0, 5);
                                    finishMove();
                                    correctMove=true;
                                }
                            }

                        } else {
                            updateArrays(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY(), 0);
                            finishMove();
                            correctMove=true;
                        }
                    } else {
                        updateArrays(getXLocation(originalContainer.getLocation()), getYLocation(originalContainer.getLocation()), availableSquares.get(i).getX(), availableSquares.get(i).getY(), 0);
                        finishMove();
                        correctMove=true;
                    }
                    break;
                } else ////if move is illegal drop piece back to its original gameBoard.square
                    if (!gameBoard.square[availableSquares.get(i).getX()][availableSquares.get(i).getY()].getName().equalsIgnoreCase(gameBoard.square[destRow][destCol].getName())) {
                        rejectedMove();
                        correctMove=false;
                    }
            }
            if (availableSquares.isEmpty()) {//if list of available gameBoard.squares for this piece is empty - no move for this piece - drop piece back to its original gameBoard.squares
                rejectedMove();
                correctMove=false;
            }
        }

        return correctMove;
    }

    /**
     * finishes accepted,legal move, checks for check,checkmate, stalemate, draw
     */
    private void finishMove() {
        String endMoveS = null;
        boolean draw=false;
        
        int blackPiecesOnBoard=0;
        int whitePiecesOnBoard=0;       
        XY blackKingPos = null;
        XY whiteKingPos = null;
        
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                if (pieces[i][j] != null) {
                    if(pieces[i][j].getPieceColor().equalsIgnoreCase("b")){
                        ++blackPiecesOnBoard;
                        if (pieces[i][j].getPieceFullName().equalsIgnoreCase("b_king")) {
                            blackKingPos = new XY(i, j);
                        }
                    }
                    if(pieces[i][j].getPieceColor().equalsIgnoreCase("w")){
                        ++whitePiecesOnBoard;
                        if (pieces[i][j].getPieceFullName().equalsIgnoreCase("w_king")) {
                            whiteKingPos = new XY(i, j);
                        }
                    }
                }
            }
        }
        
        if (blackPiecesOnBoard==1 && whitePiecesOnBoard==1){
            draw=false;
        }
        int blackKnights=0;
        int blackQueens=0;
        int blackRooks=0;
        int blackPawns=0;
        int blackBishopsB=0;
        int blackBishopsW=0;
        int whiteKnights=0;
        int whiteQueens=0;
        int whiteRooks=0;
        int whitePawns=0;
        int whiteBishopsB=0;
        int whiteBishopsW=0;
        
        
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                if (pieces[i][j] != null) {
                    if (pieces[i][j].getPieceColor().equalsIgnoreCase("b")) {
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("pawn")) {
                            ++blackPawns;
                        }
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("knight")) {
                            ++blackKnights;
                        }
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("rook")) {
                            ++blackRooks;
                        }
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("queen")) {
                            ++blackQueens;
                        }
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("bishop")) {
                            if (((i + j) % 2) == 0) {
                                ++blackBishopsW;
                            }
                            if (((i + j) % 2) == 1) {
                                ++blackBishopsB;
                            }
                        }
                    }
                    if (pieces[i][j].getPieceColor().equalsIgnoreCase("w")) {
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("pawn")) {
                            ++whitePawns;
                        }
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("knight")) {
                            ++whiteKnights;
                        }
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("rook")) {
                            ++whiteRooks;
                        }
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("queen")) {
                            ++whiteQueens;
                        }
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("bishop")) {
                            if (((i + j) % 2) == 0) {
                                ++whiteBishopsW;
                            }
                            if (((i + j) % 2) == 1) {
                                ++whiteBishopsB;
                            }
                        }
                    }
                }
            }
        }
        
        
        
        if(blackPiecesOnBoard==1 && whitePiecesOnBoard==2){
            if(whiteKnights==1){
                draw=true;
            }
            if(whiteBishopsW==1){
                draw=true;
            }
            if(whiteBishopsB==1){
                draw=true;
            }
        }
        if(whitePiecesOnBoard==1 && blackPiecesOnBoard==2){
            if(blackKnights==1){
                draw=true;
            }
            if(blackBishopsW==1){
                draw=true;
            }
            if(blackBishopsB==1){
                draw=true;
            }
        }
        
        if(whitePawns==0 && whiteQueens==0 && whiteKnights==0 && whiteRooks==0 && blackPawns==0 && blackQueens==0 && blackKnights==0 && blackRooks==0){            
            if(blackBishopsW==0 && whiteBishopsW==0){
                draw=true;
            }
            if(blackBishopsB==0 && whiteBishopsB==0){
                draw=true;
            }
        }
        
        
        
        
        
        int blackKingInCheck = 0;
        int whiteKingInCheck = 0;

        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                if (pieces[i][j] != null) {
                    if (blackKingPos != null && pieces[i][j].getPieceColor().equalsIgnoreCase("w") && !pieces[i][j].getPieceFullName().equalsIgnoreCase("w_king")) {
                        ArrayList<XY> list = new ArrayList<>();
                        list.clear();
                        list = pieces[i][j].getAvailableCheckMovesList(pieces[i][j].getPieceColor(), i, j, pieces);
                        for (int ii = 0; ii <= list.size() - 1; ++ii) {
                            int wsplX;
                            wsplX = i + (list.get(ii).getX());
                            int wsplY;
                            wsplY = j + (list.get(ii).getY());

                            if ((wsplX <= 7 && wsplX >= 0) && (wsplY <= 7 && wsplY >= 0)) {
                                if (wsplX == blackKingPos.getX() && wsplY == blackKingPos.getY()) {
                                    ++blackKingInCheck;
                                    ((King) pieces[blackKingPos.getX()][blackKingPos.getY()]).checked();
                                }
                            }
                        }
                    }
                    if (whiteKingPos != null && pieces[i][j].getPieceColor().equalsIgnoreCase("b") && !pieces[i][j].getPieceFullName().equalsIgnoreCase("b_king")) {
                        ArrayList<XY> list = new ArrayList<>();
                        list.clear();
                        list = pieces[i][j].getAvailableCheckMovesList(pieces[i][j].getPieceColor(), i, j, pieces);
                        for (int ii = 0; ii <= list.size() - 1; ++ii) {
                            int wsplX;
                            wsplX = i + (list.get(ii).getX());
                            int wsplY;
                            wsplY = j + (list.get(ii).getY());

                            if ((wsplX <= 7 && wsplX >= 0) && (wsplY <= 7 && wsplY >= 0)) {
                                if (wsplX == whiteKingPos.getX() && wsplY == whiteKingPos.getY()) {
                                    ++whiteKingInCheck;
                                    ((King) pieces[whiteKingPos.getX()][whiteKingPos.getY()]).checked();
                                }
                            }
                        }
                    }
                }
            }
        }

        int availableWhitePiecesToMove = 0;
        int availableBlackPiecesToMove = 0;

        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                if (pieces[i][j] != null) {
                    if (pieces[i][j].getPieceColor().equalsIgnoreCase("w")) {

                        ArrayList<XY> list = new ArrayList<>();
                        list.clear();

                        if (pieces[i][j].getPieceName().equalsIgnoreCase("king")) {
                            list = ((King) pieces[i][j]).getAvailableKingMovesList(pieces[i][j].getPieceColor(), i, j, pieces);
                        } else {
                            list = pieces[i][j].getAvailableMovesList(pieces[i][j].getPieceColor(), i, j, pieces);
                        }
                        if (!list.isEmpty()) {
                            ++availableWhitePiecesToMove;
                        }
                    }
                    if (pieces[i][j].getPieceColor().equalsIgnoreCase("b")) {
                        ArrayList<XY> list = new ArrayList<>();
                        list.clear();
                        if (pieces[i][j].getPieceName().equalsIgnoreCase("king")) {
                            list = ((King) pieces[i][j]).getAvailableKingMovesList(pieces[i][j].getPieceColor(), i, j, pieces);
                        } else {
                            list = pieces[i][j].getAvailableMovesList(pieces[i][j].getPieceColor(), i, j, pieces);
                        }
                        if (!list.isEmpty()) {
                            ++availableBlackPiecesToMove;
                        }
                        
                    }
                }
            }
        }
                             
        if (blackKingInCheck == 0 && availableBlackPiecesToMove == 0) {
            draw=true;
        }
        if (whiteKingInCheck == 0 && availableWhitePiecesToMove == 0) {
            draw=true;
        }
        
        
        if (blackKingInCheck == 1 && availableBlackPiecesToMove != 0) {
            endMoveS = "+";
        }
        if (blackKingInCheck == 2 && availableBlackPiecesToMove != 0) {
            endMoveS = "++";
        }
        if (whiteKingInCheck == 1 && availableWhitePiecesToMove != 0) {
            endMoveS = "+";
        }
        if (whiteKingInCheck == 2 && availableWhitePiecesToMove != 0) {
            endMoveS = "++";
        }
        
        
        if (draw==true) {
            endMoveS = ("\n");
            endMoveS = endMoveS.concat("\u00BD-\u00BD");
            gameBoard.endGame=true;
            if(timing==true){
                timer.stop();
            }
        }        
        if (blackKingInCheck != 0 && availableBlackPiecesToMove == 0) {
            endMoveS = "#";
            endMoveS = endMoveS.concat("\n");
            endMoveS = endMoveS.concat("1-0");
            gameBoard.endGame=true;
            if(timing==true){
                timer.stop();
            }
        }
        if (whiteKingInCheck != 0 && availableWhitePiecesToMove == 0) {
            endMoveS = "#";
            endMoveS = endMoveS.concat("\n");
            endMoveS = endMoveS.concat("0-1");
            gameBoard.endGame=true;
            if(timing==true){
                timer.stop();
            }
        }
        
        
        if (notation == true) {
            if (endMoveS == null) {
                if ((turns % 2) == 1) {
                    updateLogDisplay("  ");
                }
                if ((turns % 2) == 0) {
                    updateLogDisplay("\n");
                }
            } else {
                if ((turns % 2) == 1) {
                    updateLogDisplay(endMoveS + " ");
                }
                if ((turns % 2) == 0) {
                    updateLogDisplay(endMoveS + "\n");
                }
            }
        }
        
        ++turns;
        if( (turns%2)==0 ){
            ++turn;
        }
    }




    /**
     * Method for rejected move - illegal move
     */
    private void rejectedMove() {
        originalContainer.add(chessPiece);
        originalContainer.validate();
    }

    /**
     * Highlites gameBoard.squares where piece c can legally move gets location of piece
     * and transforms it to board coordinates puts in <b>movelist</b> array
     * available moves for all gameBoard.squares where piece can move border is set
     *
     * @param c piece for which method checks gameBoard.squares
     * @see pieces.Piece#getAvailableMovesList(java.lang.String, int, int,
     * pieces.Piece[][])
     *
     */
    public void highlightValidMoves(Component c) {
        

        Border outer = BorderFactory.createLoweredBevelBorder();
        Border inner = BorderFactory.createMatteBorder(4, 4, 4, 4, Color.yellow);
        Border border = BorderFactory.createCompoundBorder(outer, inner);

        for (int i = 0; i <= availableSquares.size() - 1; ++i) {
            int wsplX;
            wsplX = (availableSquares.get(i).getX());
            int wsplY;
            wsplY = (availableSquares.get(i).getY());

            if ((wsplX <= 7 && wsplX >= 0) && (wsplY <= 7 && wsplY >= 0)) {
                if (!(cell[wsplX][wsplY] != null && (((Piece) c).getPieceColor()).equalsIgnoreCase(cell[wsplX][wsplY].substring(0, 1)))) {
                    gameBoard.square[wsplX][wsplY].setBorder(border);
                } else {

                }

            }
        }
    }

    /**
     * Gets all available gameBoard.squares where piece can legally move and stores them
     * into availableSquares arrayList
     *
     * @param c piece for which get all available gameBoard.squares where piece can
     * legally move
     * @see #highlightValidMoves(java.awt.Component)
     */
    public void getAvailableSquares(Component c) {
        int x, y;
        x = getXLocation(c.getParent().getLocation());
        y = getYLocation(c.getParent().getLocation());

        moveList.clear();
        if (((Piece) c).getPieceName().equalsIgnoreCase("king")) {
            moveList = ((King) c).getAvailableKingMovesList(((Piece) c).getPieceColor(), x, y, pieces);
        } else {
            moveList = ((Piece) c).getAvailableMovesList(((Piece) c).getPieceColor(), x, y, pieces);
        }
        for (int i = 0; i <= moveList.size() - 1; ++i) {
            int wsplX;
            wsplX = x + (moveList.get(i).getX());
            int wsplY;
            wsplY = y + (moveList.get(i).getY());
            if ((wsplX <= 7 && wsplX >= 0) && (wsplY <= 7 && wsplY >= 0)) {
                if (((Piece) c).getPieceFullName().equalsIgnoreCase("b_king") /*&& whiteAttackGridIK[wsplX][wsplY] == false*/) {//if piece is white king and gameBoard.square is not uder attack allow move
                    availableSquares.add(new XY(wsplX, wsplY));
                }
                if (((Piece) c).getPieceFullName().equalsIgnoreCase("w_king")/* && blackAttackGridIK[wsplX][wsplY] == false*/) {//if piece is black king and gameBoard.square is not uder attack allow move
                    availableSquares.add(new XY(wsplX, wsplY));
                }
                if (!((Piece) c).getPieceName().equalsIgnoreCase("king")) {//if piece is not king
                    availableSquares.add(new XY(wsplX, wsplY));
                }
            }
        }

    }

    /**
     * update pieces[][] and cell[][] arrays in case piece moved from one gameBoard.square
     * to another
     *
     * @param sourceXLocation row location from where piece moved
     * @param sourceYLocation column location from where piece moved
     * @param destX row location to where piece moved
     * @param destY column location to where piece moved
     * @param var 0=empty cell; 1=capture; 2=en passant;3=promotion on
     * empty;4=promotion with capture;5=castling kingside;6=castling queenside
     */
    private void updateArrays(int sourceXLocation, int sourceYLocation, int destX, int destY, int var) {
        String firstPart = "";
        String middlePart = "";
        String secondPart = "";

        if (var != 5 && var != 6) {
            if(language.equalsIgnoreCase("pl")){
                firstPart = pieces[sourceXLocation][sourceYLocation].getPieceANPolish();
            }
            if(language.equalsIgnoreCase("en")){
                firstPart = pieces[sourceXLocation][sourceYLocation].getPieceANEnglish();
            }
            firstPart = firstPart.concat(gameBoard.square[sourceXLocation][sourceYLocation].getName());
        }

        if (var == 0) {
            middlePart = "-";
            secondPart = gameBoard.square[destX][destY].getName();
            pieces[destX][destY] = pieces[sourceXLocation][sourceYLocation];
            pieces[sourceXLocation][sourceYLocation] = null;
            cell[destX][destY] = cell[sourceXLocation][sourceYLocation];
            cell[sourceXLocation][sourceYLocation] = null;
        }

        if (var == 1) {
            middlePart = "x";
            secondPart = gameBoard.square[destX][destY].getName();
            pieces[destX][destY] = pieces[sourceXLocation][sourceYLocation];
            pieces[sourceXLocation][sourceYLocation] = null;
            cell[destX][destY] = cell[sourceXLocation][sourceYLocation];
            cell[sourceXLocation][sourceYLocation] = null;
        }
        if (var == 2) {
            middlePart = "x";
            secondPart = gameBoard.square[destX][destY].getName();
            secondPart = secondPart.concat("e.p.");
            if (pieces[sourceXLocation][sourceYLocation].getPieceColor().equalsIgnoreCase("b")) {
                pieces[destX - 1][destY] = null;
                cell[destX - 1][destY] = null;
            }
            if (pieces[sourceXLocation][sourceYLocation].getPieceColor().equalsIgnoreCase("w")) {
                pieces[destX + 1][destY] = null;
                cell[destX + 1][destY] = null;
            }
            pieces[destX][destY] = pieces[sourceXLocation][sourceYLocation];
            pieces[sourceXLocation][sourceYLocation] = null;
            cell[destX][destY] = cell[sourceXLocation][sourceYLocation];
            cell[sourceXLocation][sourceYLocation] = null;
        }

        if (var == 3) {
            middlePart = "-";
            secondPart = gameBoard.square[destX][destY].getName();
            pieces[destX][destY] = pieces[sourceXLocation][sourceYLocation];
            pieces[sourceXLocation][sourceYLocation] = null;
            cell[destX][destY] = cell[sourceXLocation][sourceYLocation];
            cell[sourceXLocation][sourceYLocation] = null;
            secondPart = secondPart.concat("=");
            if(language.equalsIgnoreCase("pl")){
                secondPart = secondPart.concat(pieces[destX][destY].getPieceANPolish());
            }
            if(language.equalsIgnoreCase("en")){
                secondPart = secondPart.concat(pieces[destX][destY].getPieceANEnglish());
            }
        }
        if (var == 4) {
            middlePart = "x";
            secondPart = gameBoard.square[destX][destY].getName();
            pieces[destX][destY] = pieces[sourceXLocation][sourceYLocation];
            pieces[sourceXLocation][sourceYLocation] = null;
            cell[destX][destY] = cell[sourceXLocation][sourceYLocation];
            cell[sourceXLocation][sourceYLocation] = null;
            secondPart = secondPart.concat("=");
            if(language.equalsIgnoreCase("pl")){
                secondPart = secondPart.concat(pieces[destX][destY].getPieceANPolish());
            }
            if(language.equalsIgnoreCase("en")){
                secondPart = secondPart.concat(pieces[destX][destY].getPieceANEnglish());
            }
        }
        if (var == 5) {
            firstPart = "0-0";
            middlePart = "";
            secondPart = "";
        }
        if (var == 6) {
            firstPart = "0-0-0";
            middlePart = "";
            secondPart = "";
        }
        
        
        if (var != 5 && var != 6) {
            pieces[destX][destY].moved();
        }
        if(notation==true){
            try {
                if((turns%2)==1){
                    logDisplay.addText(turn+". "+firstPart + middlePart + secondPart);
                }
                if((turns%2)==0){
                    logDisplay.addText(firstPart + middlePart + secondPart);
                }
            } catch (BadLocationException ex) {
                Logger.getLogger(GameBoardLogic.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    /**
     * updates chess annotation display
     * @param string    String which to display
     */
    private void updateLogDisplay(String string) {
        String s = string;

        try {
            logDisplay.addText(s);
        } catch (BadLocationException ex) {
            Logger.getLogger(GameBoardLogic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * populates board with pieces
     */
    public void resetBoard() {
        if(notation==true){
            logDisplay.clear();
        }
        if(timing==true){
            timer.resetTime();
        }
        
        gameBoard.resetTurns();
        turn=1;
        turns=1;        
        gameBoard.endGame=false;
        
        resetBoardsArraysFlags();
        unHighlightAllMoves();
        
        
        for (int i = 0; i <= 7; ++i) {
        Piece piece = new Pawn("w");
        gameBoard.square[6][i].add((Component) piece);
        cell[6][i] = piece.getPieceFullName();
        pieces[6][i] = piece;
        }

        for (int i = 0; i <= 7; ++i) {
        Piece piece = new Pawn("b");
        gameBoard.square[1][i].add((Component) piece);
        cell[1][i] = piece.getPieceFullName();
        pieces[1][i] = piece;
        }
        for (int i = 0; i <= 7; ++i) {
            Piece piece=null;
            if(i==0 || i==7){
                piece = new Rook("b");
            }
            if(i==1 || i==6){
                piece = new Knight("b");
            }
            if(i==2 || i==5){
                piece = new Bishop("b");
            }
            if(i==3){
                piece = new Queen("b");
            }
            if(i==4){
                piece = new King("b");
            }
            gameBoard.square[0][i].add((Component) piece);
            cell[0][i] = piece.getPieceFullName();
            pieces[0][i] = piece;
        }        
        for (int i = 0; i <= 7; ++i) {
            Piece piece=null;
            if(i==0 || i==7){
                piece = new Rook("w");
            }
            if(i==1 || i==6){
                piece = new Knight("w");
            }
            if(i==2 || i==5){
                piece = new Bishop("w");
            }
            if(i==3){
                piece = new Queen("w");
            }
            if(i==4){
                piece = new King("w");
            }       
            gameBoard.square[7][i].add((Component) piece);
            cell[7][i] = piece.getPieceFullName();
            pieces[7][i] = piece;
        }
        
        
        
        gameBoard.validate();
    }
    
    
    public void setBoard(int turn,int turns, String[][] cell, Piece[][] pieces) {
        if(notation==true){
            logDisplay.clear();
        }
        if(timing==true){
            timer.resetTime();
        }
        
        this.turn=turn;
        this.turns=turns;        
        gameBoard.endGame=false;
        
        resetBoardsArraysFlags();
        unHighlightAllMoves();
        
        this.cell=cell;
        this.pieces=pieces;
        
        Piece piece;
         for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                piece=pieces[i][j];
                if(piece!=null)
                    gameBoard.square[i][j].add( (Component) piece);
            }
         }
        
    }

    /**
     * interprets x coordinate from location on board to row number
     *
     * @param p point from which get x coordinate
     * @return row number
     */
    private int getXLocation(Point p) {
        int x, px;
        px = (int) p.getY();
        x = px / (gameBoard.size/8);
        return x;
    }

    /**
     * interprets y coordinate from location on board to column number
     *
     * @param p point from which get y coordinate
     * @return column number
     */
    private int getYLocation(Point p) {
        int y, py;
        py = (int) p.getX();
        y = py / (gameBoard.size/8);
        return y;
    }

    /**
     * unhighlites all gameBoard.squares on board
     */
    public void unHighlightAllMoves() {
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                gameBoard.square[i][j].setBorder(null);
            }
        }
        availableSquares.clear();
    }

    /**
     * Checks if move which was performed by pwn was en passant move
     *
     * @param xOrgLocation row number of original location of piece
     * @param yOrgLocation column number of original location of piece
     * @param xDest row number of destination location of piece
     * @param yDest column number of destination location of piece
     * @return true if move was en passant move
     */
    private boolean checkEnPassant(int xOrgLocation, int yOrgLocation, int xDest, int yDest) {
        int x = xOrgLocation;
        int y = yOrgLocation;
        int xD = xDest;
        int yD = yDest;

        if (pieces[x][y].getPieceFullName().equalsIgnoreCase("w_pawn") && x == 3 && (y - 1) >= 0 && pieces[3][y - 1] != null && xD == 2 && yD == y - 1 && pieces[xD][yD] == null && pieces[3][y - 1].getPieceFullName().equalsIgnoreCase("b_pawn") && ((Pawn) pieces[3][y - 1]).isTwoSquareMoved()) {
            return true;
        }
        if (pieces[x][y].getPieceFullName().equalsIgnoreCase("w_pawn") && x == 3 && (y + 1) <= 7 && pieces[3][y + 1] != null && xD == 2 && yD == y + 1 && pieces[xD][yD] == null && pieces[3][y + 1].getPieceFullName().equalsIgnoreCase("b_pawn") && ((Pawn) pieces[3][y + 1]).isTwoSquareMoved()) {
            return true;
        }

        if (pieces[x][y].getPieceFullName().equalsIgnoreCase("b_pawn") && x == 4 && (y - 1) >= 0 && pieces[4][y - 1] != null && xD == 5 && yD == y - 1 && pieces[xD][yD] == null && pieces[4][y - 1].getPieceFullName().equalsIgnoreCase("w_pawn") && ((Pawn) pieces[4][y - 1]).isTwoSquareMoved()) {
            return true;
        }
        if (pieces[x][y].getPieceFullName().equalsIgnoreCase("b_pawn") && x == 4 && (y + 1) <= 7 && pieces[4][y + 1] != null && xD == 5 && yD == y + 1 && pieces[xD][yD] == null && pieces[4][y + 1].getPieceFullName().equalsIgnoreCase("w_pawn") && ((Pawn) pieces[4][y + 1]).isTwoSquareMoved()) {
            return true;
        }

        return false;
    }

    /**
     * Clears all board and arrays
     */
    private void resetBoardsArraysFlags() {
        gameBoard.clearBoard();
        cell = new String[8][8];
        pieces = new Piece[8][8];
        moveList.clear();
        availableSquares.clear();
    }
    
    
    public SaveFileData getSaveData(){
        
        SaveFileData data = new SaveFileData();
        data.setNotationFlag(notation);
        data.setLanguage(language);
        data.setTimerFlag(timing);
        if(timing==true){
            data.setWhitePlayerTime(timer.getWhitePlayerTime());
            data.setBlackPlayerTime(timer.getBlackPlayerTime());
            data.setTimeFlow(timeFlow);
        }
        data.setTurn(turn);
        data.setTurns(turns);
        if(notation==true){
            data.setLogText(logDisplay.getText());
        }
        data.setWhosTurn(gameBoard.turn);
        
        data.setCell(cell);
        data.setPieces(pieces);
        
        /*
        System.out.print(pieces[0][0]);
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                System.out.print("|");
                System.out.print(cell[i][j]);
            }
            System.out.println("|");
        }*/
        return data;
    }

    public boolean isEnd() {
        return gameBoard.endGame;
    }


    
    /**
     * prints all arrays to console
     *//*
    private void printArrays() {        
        System.out.println("-------");
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                System.out.print("|");
                System.out.print(cell[i][j]);
            }
            System.out.println("|");
        }
        System.out.println("-------");
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                System.out.print("|");
                if (pieces[i][j] != null) {
                    System.out.print(pieces[i][j].getPieceFullName());
                } else {
                    System.out.print("null");
                }
            }
            System.out.println("|");
        }
    }*/
}
