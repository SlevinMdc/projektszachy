package org;

import GUI.Frame;
import javax.swing.SwingUtilities;


/**
 * 
 * @author TOM-12  <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here      
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Frame f = new Frame();
                f.setVisible(true);
            }
        });
        
    }
    
}
