/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org;

import GUI.LogDisplayText;
import GUI.TimerPanel;

/**
 *
 * @author TOM-12 <tomasz12@gmail.com>
 */
public class Parameters {
    
    private final String lang;
    private final LogDisplayText logDisplay;
    private final TimerPanel timer;
    private final String timeFlow;
    private Object board;
    private String boardClassName;    
    
    /**
     * Constructor for object prameters
     * @param lang  lang "en" or "pl"
     * @param logDisplay    instance of log display
     * @param timer instance of timer panel
     * @param timeFlow time flow "down" or "up"
     */
    public Parameters(String lang,LogDisplayText logDisplay,TimerPanel timer,String timeFlow) {
        this.lang=lang;
        this.logDisplay=logDisplay;
        this.timer = timer;
        this.timeFlow=timeFlow;
    }

    /**
     *
     * @return object board
     */
    public Object getBoard() {
        return board;
    }

    /**
     *
     * @return boards name
     */
    public String getBoardClassName() {
        return boardClassName;
    }

    /**
     *
     * @param board
     * @param boardClassName
     */
    public void setBoard(Object board,String boardClassName) {
        this.boardClassName=boardClassName;
        this.board = board;
    }

    /**
     *
     * @return language 
     */
    public String getLang() {
        return lang;
    }

    /**
     *
     * @return display for chess notation
     */
    public LogDisplayText getLogDisplay() {
        return logDisplay;
    }

    /**
     *
     * @return timer
     */
    public TimerPanel getTimer() {
        return timer;
    }

    /**
     *
     * @return time flow
     */
    public String getTimeFlow() {
        return timeFlow;
    }
    
    /**
     *
     * @return true if lang was defined
     */
    public boolean isLang() {
        return lang!=null;
    }

    /**
     *
     * @return true if defined
     */
    public boolean isLogDisplay() {
        return logDisplay!=null;
    }

    /**
     *
     * @return true if defined
     */
    public boolean isTimer() {
        return timer!=null;
    }

    /**
     *
     * @return true if defined
     */
    public boolean isTimeFlow() {
        return timeFlow!=null;
    }
    
}
