/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org;

import java.io.Serializable;
import pieces.Piece;

/**
 *
 * @author TOM-12 <tomasz12@gmail.com>
 */
public class SaveFileData implements Serializable {

    private boolean notationFlag;
    private boolean timerFlag;
    private String timeFlow;
    private boolean timeFlowBoolean;
    private int turn;
    private int turns;
    private String logText;
    private String language;
    private int blackPlayerTime;
    private int whitePlayerTime;
    private String cell[][] = new String[8][8];
    private Piece pieces[][] = new Piece[8][8];
    private String whosTurn;

    public SaveFileData() {

    }

    public String getWhosTurn() {
        return whosTurn;
    }

    public void setWhosTurn(String whosTurn) {
        this.whosTurn = whosTurn;
    }

    
    
    void setNotationFlag(boolean notationFlag) {
        this.notationFlag = notationFlag;
    }

    void setTimerFlag(boolean timingFlag) {
        this.timerFlag = timingFlag;
    }

    void setLanguage(String language) {
        switch (language) {
            case "Angielska notacja":
                language = "en";
                break;
            case "Polska notacja":
                language = "pl";
                break;
        }
        this.language = language;
    }

    void setTimeFlow(String timeFlow) {        
        switch (timeFlow) {
            case "Odliczanie w górę":
                timeFlow = "up";
                break;
            case "Odliczanie w dół":
                timeFlow = "down";
                break;
        }        
        this.timeFlow = timeFlow;
        setTimeFlowBoolean();
    }

    public void setTimeFlowBoolean() {

        boolean flowB = false;
        switch (timeFlow) {
            case "Odliczanie w górę":
                flowB = true;
                break;
            case "Odliczanie w dół":
                flowB = false;
                break;
        }
        this.timeFlowBoolean = flowB;
    }

    void setBlackPlayerTime(int blackPlayerTime) {
        this.blackPlayerTime = blackPlayerTime;
    }

    void setWhitePlayerTime(int whitePlayerTime) {
        this.whitePlayerTime = whitePlayerTime;
    }

    void setLogText(String text) {
        this.logText = text;
    }

    void setTurns(int turns) {
        this.turns = turns;
    }

    void setTurn(int turn) {
        this.turn = turn;
    }

    public boolean isNotationFlag() {
        return notationFlag;
    }

    public boolean isTimerFlag() {
        return timerFlag;
    }

    public boolean isTimeFlowBoolean() {
        return timeFlowBoolean;
    }

    public String getTimeFlow() {
        return timeFlow;
    }

    public int getTurn() {
        return turn;
    }

    public int getTurns() {
        return turns;
    }

    public String getLogText() {
        return logText;
    }

    public String getLanguage() {
        return language;
    }

    public int getBlackPlayerTime() {
        return blackPlayerTime;
    }

    public int getWhitePlayerTime() {
        return whitePlayerTime;
    }

    public String[][] getCell() {
        return cell;
    }

    public void setCell(String[][] cell) {
        this.cell = cell;
    }

    public Piece[][] getPieces() {
        return pieces;
    }

    public void setPieces(Piece[][] pieces) {
        this.pieces = pieces;
    }

}
