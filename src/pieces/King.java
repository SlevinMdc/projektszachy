/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;

import java.awt.Dimension;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JLabel;

/**
 *
 * @author TOM-12 <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class King extends JLabel implements Piece,Serializable{

    private final ArrayList<XY> availableCheckMoves = new ArrayList<>();
    private final ArrayList<XY> availableMoves = new ArrayList<>();
    private boolean moved;
    private final String name = "king";
    private final  String fullName;
    private final String color;
    private boolean obstacleNE;
    private boolean obstacleSE;
    private boolean obstacleSW;
    private boolean obstacleNW;
    private boolean obstacleN;
    private boolean obstacleE;
    private boolean obstacleS;
    private boolean obstacleW;
    private boolean check;
    private boolean castling=false;
    /**
     * Constructs king piece of specified color
     *
     * @param c color of king("w" if white "b" if black)
     */
    public King(String c) {
        color = c;
        setMinimumSize(new Dimension(50, 50));
        setPreferredSize(new Dimension(50, 50));
        fullName = color.concat("_king");
        setName(name);
        setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/" + fullName + ".png")));
        moved = false;
        check = false;
        
    }

    /**
     * returns ArrayList of all available king moves
     * @param color kings color
     * @param x kings x position
     * @param y kings y position
     * @param pieces array of existing pieces on board
     * @return ArrayList of available king moves
     */
    public ArrayList getAvailableKingMovesList(String color, int x, int y, Piece[][] pieces) {
        availableMoves.clear();
        obstacleNE = false;
        obstacleSE = false;
        obstacleSW = false;
        obstacleNW = false;
        obstacleN = false;
        obstacleE = false;
        obstacleS = false;
        obstacleW = false;

        if ((x - 2) >= 0 && (y - 2) >= 0 && pieces[x - 2][y - 2] != null && pieces[x - 2][y - 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleNW = true;
        }
        if ((x - 2) >= 0 && (y - 1) >= 0 && pieces[x - 2][y - 1] != null && pieces[x - 2][y - 1].getPieceName().equalsIgnoreCase("king")) {
            obstacleNW = true;
            obstacleN = true;
        }
        if ((x - 2) >= 0 && pieces[x - 2][y] != null && pieces[x - 2][y].getPieceName().equalsIgnoreCase("king")) {
            obstacleNW = true;
            obstacleN = true;
            obstacleNE = true;
        }
        if ((x - 2) >= 0 && (y + 1) <= 7 && pieces[x - 2][y + 1] != null && pieces[x - 2][y + 1].getPieceName().equalsIgnoreCase("king")) {
            obstacleN = true;
            obstacleNE = true;
        }
        if ((x - 2) >= 0 && (y + 2) <= 7 && pieces[x - 2][y + 2] != null && pieces[x - 2][y + 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleNE = true;
        }
        if ((x - 1) >= 0 && (y + 2) <= 7 && pieces[x - 1][y + 2] != null && pieces[x - 1][y + 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleNE = true;
            obstacleE = true;
        }
        if ((y + 2) <= 7 && pieces[x][y + 2] != null && pieces[x][y + 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleNE = true;
            obstacleE = true;
            obstacleSE = true;
        }
        if ((x + 1) <= 7 && (y + 2) <= 7 && pieces[x + 1][y + 2] != null && pieces[x + 1][y + 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleE = true;
            obstacleSE = true;
        }
        if ((x + 2) <= 7 && (y + 2) <= 7 && pieces[x + 2][y + 2] != null && pieces[x + 2][y + 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleSE = true;
        }
        if ((x + 2) <= 7 && (y + 1) <= 7 && pieces[x + 2][y + 1] != null && pieces[x + 2][y + 1].getPieceName().equalsIgnoreCase("king")) {
            obstacleS = true;
            obstacleSE = true;
        }
        if ((x + 2) <= 7 && pieces[x + 2][y] != null && pieces[x + 2][y].getPieceName().equalsIgnoreCase("king")) {
            obstacleSE = true;
            obstacleS = true;
            obstacleSW = true;
        }
        if ((x + 2) <= 7 && (y - 1) >= 0 && pieces[x + 2][y - 1] != null && pieces[x + 2][y - 1].getPieceName().equalsIgnoreCase("king")) {
            obstacleS = true;
            obstacleSW = true;
        }
        if ((x + 2) <= 7 && (y - 2) >= 0 && pieces[x + 2][y - 2] != null && pieces[x + 2][y - 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleSW = true;
        }
        if ((x + 1) <= 7 && (y - 2) >= 0 && pieces[x + 1][y - 2] != null && pieces[x + 1][y - 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleW = true;
            obstacleSW = true;
        }
        if ((y - 2) >= 0 && pieces[x][y - 2] != null && pieces[x][y - 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleNW = true;
            obstacleW = true;
            obstacleSW = true;
        }
        if ((x - 1) >= 0 && (y - 2) >= 0 && pieces[x - 1][y - 2] != null && pieces[x - 1][y - 2].getPieceName().equalsIgnoreCase("king")) {
            obstacleNW = true;
            obstacleW = true;
        }

        
        if ((x - 1) >= 0 && (x - 1) <= 7 && (y + 1) <= 7 && (y + 1) >= 0) {
            if (obstacleNE == false) {
                if ((pieces[x - 1][y + 1] == null)) {
                    if(checkIfSqureUnderAttack(x-1,y+1,pieces)==false){
                        availableMoves.add(new XY(-1, +1));
                    }
                } else {
                    if (pieces[x - 1][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                    }
                    if (!pieces[x - 1][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                        if(checkIfSqureUnderAttack(x-1,y+1,pieces)==false){
                            availableMoves.add(new XY(-1, +1));
                        }
                    }
                }
            }
        }

        if ((x + 1) >= 0 && (x + 1) <= 7 && (y + 1) <= 7 && (y + 1) >= 0) {
            if (obstacleSE == false) {
                if ((pieces[x + 1][y + 1] == null)) {
                    if(checkIfSqureUnderAttack(x+1,y+1,pieces)==false){
                        availableMoves.add(new XY(+1, +1));
                    }
                } else {
                    if (pieces[x + 1][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                    }
                    if (!pieces[x + 1][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                        if(checkIfSqureUnderAttack(x+1,y+1,pieces)==false){
                            availableMoves.add(new XY(+1, +1));
                        }
                    }
                }
            }
        }

        if ((x + 1) >= 0 && (x + 1) <= 7 && (y - 1) <= 7 && (y - 1) >= 0) {
            if (obstacleSW == false) {
                if ((pieces[x + 1][y - 1] == null)) {
                    if(checkIfSqureUnderAttack(x+1,y-1,pieces)==false){
                        availableMoves.add(new XY(+1, -1));
                    }
                } else {
                    if (pieces[x + 1][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                    }
                    if (!pieces[x + 1][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                        if(checkIfSqureUnderAttack(x+1,y-1,pieces)==false){
                            availableMoves.add(new XY(+1, -1));
                        }
                    }
                }
            }
        }

        if ((x - 1) >= 0 && (x - 1) <= 7 && (y - 1) <= 7 && (y - 1) >= 0) {
            if (obstacleNW == false) {
                if ((pieces[x - 1][y - 1] == null)) {
                    if(checkIfSqureUnderAttack(x-1,y-1,pieces)==false){
                            availableMoves.add(new XY(-1, -1));
                    }
                } else {
                    if (pieces[x - 1][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                    }
                    if (!pieces[x - 1][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                        if(checkIfSqureUnderAttack(x-1,y-1,pieces)==false){ 
                                availableMoves.add(new XY(-1, -1));
                        }
                    }
                }
            }
        }
        //n e s w 
        if ((x - 1) >= 0 && (x - 1) <= 7) {
            if (obstacleN == false) {
                if ((pieces[x - 1][y] == null)) {
                    if(checkIfSqureUnderAttack(x-1,y,pieces)==false){ 
                        availableMoves.add(new XY(-1, 0));
                    }
                } else {
                    if (pieces[x - 1][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                    }
                    if (!pieces[x - 1][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                        if(checkIfSqureUnderAttack(x-1,y,pieces)==false){
                            availableMoves.add(new XY(-1, 0));
                        }
                    }
                }
            }
        }

        if ((y + 1) <= 7 && (y + 1) >= 0) {
            if (obstacleE == false) {
                if ((pieces[x][y + 1] == null)) {
                    if(checkIfSqureUnderAttack(x,y+1,pieces)==false){   
                        availableMoves.add(new XY(0, +1));
                    }
                } else {
                    if (pieces[x][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                    }
                    if (!pieces[x][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                        if(checkIfSqureUnderAttack(x,y+1,pieces)==false){
                            availableMoves.add(new XY(0, +1));
                        }
                    }
                }
            }
        }

        if ((x + 1) >= 0 && (x + 1) <= 7) {
            if (obstacleS == false) {
                if ((pieces[x + 1][y] == null)) {
                    if(checkIfSqureUnderAttack(x+1,y,pieces)==false){
                        availableMoves.add(new XY(+1, 0));
                    }
                } else {
                    if (pieces[x + 1][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                    }
                    if (!pieces[x + 1][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                        if(checkIfSqureUnderAttack(x+1,y,pieces)==false){
                            availableMoves.add(new XY(+1, 0));
                        }
                    }
                }
            }
        }

        if ((y - 1) <= 7 && (y - 1) >= 0) {
            if (obstacleW == false) {
                if ((pieces[x][y - 1] == null)) {
                    if(checkIfSqureUnderAttack(x,y-1,pieces)==false){
                        availableMoves.add(new XY(0, -1));
                    }
                } else {
                    if (pieces[x][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                    }
                    if (!pieces[x][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                        if(checkIfSqureUnderAttack(x,y-1,pieces)==false){
                            availableMoves.add(new XY(0, -1));
                        }
                    }
                }
            }
        }
        
        if(pieces[x][y].isMoved()==false && ((King)pieces[x][y]).isChecked()==false ){
            if(color.equalsIgnoreCase("w") && x==7){
                if(pieces[7][0]!=null && pieces[7][0].getPieceFullName().equalsIgnoreCase("w_rook") && pieces[7][0].isMoved()==false){
                    if(pieces[7][1]==null && pieces[7][2]==null && pieces[7][3]==null && checkIfSqureUnderAttack(7,1,pieces)==false && checkIfSqureUnderAttack(7,2,pieces)==false && checkIfSqureUnderAttack(7,3,pieces)==false){
                        availableMoves.add(new XY(0, -2));
                        canCastling();
                    }
                }
                if(pieces[7][7]!=null && pieces[7][7].getPieceFullName().equalsIgnoreCase("w_rook") && pieces[7][7].isMoved()==false){
                    if(pieces[7][5]==null && pieces[7][6]==null && checkIfSqureUnderAttack(7,5,pieces)==false && checkIfSqureUnderAttack(7,6,pieces)==false){
                        availableMoves.add(new XY(0, +2));
                        canCastling();
                    }
                }
            }if(color.equalsIgnoreCase("b") && x==0){
                if(pieces[0][0]!=null && pieces[0][0].getPieceFullName().equalsIgnoreCase("b_rook") && pieces[0][0].isMoved()==false){
                    if(pieces[0][1]==null && pieces[0][2]==null && pieces[0][3]==null && checkIfSqureUnderAttack(0,1,pieces)==false && checkIfSqureUnderAttack(0,2,pieces)==false && checkIfSqureUnderAttack(0,3,pieces)==false){
                        availableMoves.add(new XY(0, -2));
                        canCastling();
                    }
                }
                if(pieces[0][7]!=null && pieces[0][7].getPieceFullName().equalsIgnoreCase("b_rook") && pieces[0][7].isMoved()==false){
                    if(pieces[0][5]==null && pieces[0][6]==null && checkIfSqureUnderAttack(0,5,pieces)==false && checkIfSqureUnderAttack(0,6,pieces)==false){
                        availableMoves.add(new XY(0, +2));
                        canCastling();
                    }
                }
            }
        }
        
        
        
        
        return availableMoves;
    }
    
    private boolean checkIfSqureUnderAttack(int row,int col,Piece[][] pieces){
        boolean fl = false;
        Piece[][] tmp = new Piece[8][8];
        ArrayList tmpList=new ArrayList();
        tmpList.clear();
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                tmp[i][j] = pieces[i][j];
            }
        }
        
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j){
                if (tmp[i][j]!=null && !tmp[i][j].getPieceColor().equalsIgnoreCase(color) && !tmp[i][j].getPieceName().equalsIgnoreCase("king")  ){
                    tmpList=tmp[i][j].getAvailableMovesListIgnoreKingAndColor(tmp[i][j].getPieceColor(), i, j, tmp);
                    if (!tmpList.isEmpty()) {
                        for (int ii = 0; ii <= tmpList.size() - 1; ++ii) {
                            int wsplX;
                            wsplX = i + (((XY) tmpList.get(ii)).getX());
                            int wsplY;
                            wsplY = j + (((XY) tmpList.get(ii)).getY());
                            if ((wsplX <= 7 && wsplX >= 0) && (wsplY <= 7 && wsplY >= 0)) {
                                if (wsplX==row && wsplY==col) {
                                    fl = true;
                                    break;
                                }
                            }
                        }
                    } else if (tmpList.isEmpty()) {
                    }
                }
            }
        }
        
        
        
        return fl;
    }
    
    
    @Override
    public ArrayList getAvailableMovesList(String color, int x, int y, Piece[][] pieces) {
        availableMoves.clear();
        return availableMoves;
    }
    
    /**
     * changes variable responsible for determinig if king can castle
     */
    private void canCastling(){
        castling=true;
    }
    
    /**
     * changes variable responsible for determinig if king can castle
     */
    public void castlingExe(){
        castling=false;
    }

    /**
     * returns true if king can castle
     * @return boolean
     */
    public boolean isCastling(){
        return castling;
    }
    @Override
    public void moved() {
        moved = true;
    }

    @Override
    public boolean isMoved() {
        return moved;
    }

    @Override
    public String getPieceName() {
        return name;
    }

    @Override
    public String getPieceColor() {
        return color;
    }

    @Override
    public String getPieceFullName() {
        return fullName;
    }

    @Override
    public String getPieceANEnglish() {
        return "K";
    }

    @Override
    public String getPieceANPolish() {
        return "K";
    }

    @Override
    public ArrayList getAvailableCheckMovesList(String color, int x, int y, Piece[][] pieces) {
        availableCheckMoves.clear();
        return availableCheckMoves;
    }

    /**
     * changes variable responsible for determinig if king under check
     */
    public void checked() {
        check = true;
    }

    /**
     * changes variable responsible for determinig if king under check
     */
    public void unChecked() {
        check = false;
    }
    /**
     * returns true if this king is under checked
     * @return boolean
     */
    public boolean isChecked() {
        return check;
    }

    @Override
    public ArrayList getAvailableMovesListIgnoreKingAndColor(String color, int x, int y, Piece[][] pieces) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean checkIfMovePlacesInCheck(int x1, int y1, int a, int b, Piece[][] pieces) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
