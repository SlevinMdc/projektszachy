/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;

import java.awt.Dimension;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JLabel;

/**
 *
 * @author TOM-12 <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class Knight extends JLabel implements Piece,Serializable{

    private final ArrayList<XY> availableCheckMoves = new ArrayList<>();
    private final ArrayList<XY> availableMoves = new ArrayList<>();
    private boolean moved;
    private final String name = "knight";
    private final String fullName;
    private final String color;

    /**
     * Constructs knight piece of specified color
     *
     * @param c color of knight("w" if white "b" if black)
     */
    public Knight(String c) {
        color = c;
        setMinimumSize(new Dimension(50, 50));
        setPreferredSize(new Dimension(50, 50));
        fullName = color.concat("_knight");
        setName(name);
        setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/" + fullName + ".png")));
        moved = false;
    }

    @Override
    public boolean checkIfMovePlacesInCheck(int x1,int y1,int a,int b,Piece[][] pieces) {
        boolean fl = false;
        Piece[][] tmp = new Piece[8][8];  
        ArrayList tmpMoveList=new ArrayList();
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                tmp[i][j] = pieces[i][j];
            }
        }
        tmp[x1 + a][y1 + b] = tmp[x1][y1];
        tmp[x1][y1] = null;
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                if (tmp[i][j] != null && !tmp[i][j].getPieceColor().equalsIgnoreCase(color)) {
                    if(!tmp[i][j].getPieceName().equalsIgnoreCase("king")){
                        tmpMoveList = tmp[i][j].getAvailableCheckMovesList(tmp[i][j].getPieceColor(), i, j, tmp);
                    }
                    else{
                        tmpMoveList.clear();
                    }
                    if (!tmpMoveList.isEmpty()) {
                        for (int ii = 0; ii <= tmpMoveList.size() - 1; ++ii) {
                            int wsplX;
                            wsplX = i + (((XY) tmpMoveList.get(ii)).getX());
                            int wsplY;
                            wsplY = j + (((XY) tmpMoveList.get(ii)).getY());
                            if ((wsplX <= 7 && wsplX >= 0) && (wsplY <= 7 && wsplY >= 0)) {
                                if (tmp[wsplX][wsplY] != null && tmp[wsplX][wsplY].getPieceFullName().equalsIgnoreCase("w_king") && tmp[i][j].getPieceColor().equalsIgnoreCase("b")) {
                                    fl = true;
                                    break;//if(wsplX==7 && wsplY==4)System.out.println("true");
                                }
                                if (tmp[wsplX][wsplY] != null && tmp[wsplX][wsplY].getPieceFullName().equalsIgnoreCase("b_king") && tmp[i][j].getPieceColor().equalsIgnoreCase("w")) {
                                    fl = true;
                                    break;
                                }
                            }
                        }
                    } else if(tmpMoveList.isEmpty()){
                    }
                }
            }
        }
        return fl;
    }

    @Override
    public ArrayList getAvailableMovesList(String color, int x, int y, Piece[][] pieces) {
        availableMoves.clear();
        
        boolean flag;
        
        if ((x - 2) >= 0 && (x - 2) <= 7 && (y + 1) >= 0 && (y + 1) <= 7) {
            if (pieces[x - 2][y + 1] == null || !(pieces[x - 2][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                flag=checkIfMovePlacesInCheck(x,y,-2,+1,pieces);                
                if (flag == false) {
                    availableMoves.add(new XY(-2, 1));
                }
            }
        }
        if ((x - 1) >= 0 && (x - 1) <= 7 && (y + 2) >= 0 && (y + 2) <= 7) {
            if (pieces[x - 1][y + 2] == null || !(pieces[x - 1][y + 2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                flag=checkIfMovePlacesInCheck(x,y,-1,+2,pieces);
                if (flag == false) {
                    availableMoves.add(new XY(-1, 2));
                }
            }
        }
        if ((x + 1) >= 0 && (x + 1) <= 7 && (y + 2) >= 0 && (y + 2) <= 7) {
            if (pieces[x + 1][y + 2] == null || !(pieces[x + 1][y + 2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                flag=checkIfMovePlacesInCheck(x,y,+1,+2,pieces);
                if (flag == false) {
                    availableMoves.add(new XY(1, 2));
                }
            }
        }
        if ((x + 2) >= 0 && (x + 2) <= 7 && (y + 1) >= 0 && (y + 1) <= 7) {
            if (pieces[x + 2][y + 1] == null || !(pieces[x + 2][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                flag=checkIfMovePlacesInCheck(x,y,+2,+1,pieces);
                if (flag == false) {
                    availableMoves.add(new XY(2, 1));
                }
            }
        }
        if ((x + 2) >= 0 && (x + 2) <= 7 && (y - 1) >= 0 && (y - 1) <= 7) {
            if (pieces[x + 2][y - 1] == null || !(pieces[x + 2][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                flag=checkIfMovePlacesInCheck(x,y,2,-1,pieces);
                if (flag == false) {
                    availableMoves.add(new XY(2, -1));
                }
            }
        }
        if ((x + 1) >= 0 && (x + 1) <= 7 && (y - 2) >= 0 && (y - 2) <= 7) {
            if (pieces[x + 1][y - 2] == null || !(pieces[x + 1][y - 2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                flag=checkIfMovePlacesInCheck(x,y,1,-2,pieces);
                if (flag == false) {
                    availableMoves.add(new XY(1, -2));
                }
            }
        }
        if ((x - 1) >= 0 && (x - 1) <= 7 && (y - 2) >= 0 && (y - 2) <= 7) {
            if (pieces[x - 1][y - 2] == null || !(pieces[x - 1][y - 2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                flag=checkIfMovePlacesInCheck(x,y,-1,-2,pieces);
                if (flag == false) {
                    availableMoves.add(new XY(-1, -2));
                }
            }
        }
        if ((x - 2) >= 0 && (x - 2) <= 7 && (y - 1) >= 0 && (y - 1) <= 7) {
            if (pieces[x - 2][y - 1] == null || !(pieces[x - 2][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                flag=checkIfMovePlacesInCheck(x,y,-2,-1,pieces);
                if (flag == false) {
                    availableMoves.add(new XY(-2, -1));
                }
            }
        }
        return availableMoves;
    }

    @Override
    public void moved() {
        moved = true;
    }

    @Override
    public boolean isMoved() {
        return moved;
    }

    @Override
    public String getPieceName() {
        return name;
    }

    @Override
    public String getPieceColor() {
        return color;
    }

    @Override
    public String getPieceFullName() {
        return fullName;
    }

    @Override
    public String getPieceANEnglish() {
        return "N";
    }

    @Override
    public String getPieceANPolish() {
        return "S";
    }

    @Override
    public ArrayList getAvailableCheckMovesList(String color, int x, int y, Piece[][] pieces) {
        availableCheckMoves.clear();

        if ((x - 2) >= 0 && (x - 2) <= 7 && (y + 1) >= 0 && (y + 1) <= 7) {
            if (pieces[x - 2][y + 1] == null || !(pieces[x - 2][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                availableCheckMoves.add(new XY(-2, 1));
            }
        }
        if ((x - 1) >= 0 && (x - 1) <= 7 && (y + 2) >= 0 && (y + 2) <= 7) {
            if (pieces[x - 1][y + 2] == null || !(pieces[x - 1][y + 2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                availableCheckMoves.add(new XY(-1, 2));
            }
        }
        if ((x + 1) >= 0 && (x + 1) <= 7 && (y + 2) >= 0 && (y + 2) <= 7) {
            if (pieces[x + 1][y + 2] == null || !(pieces[x + 1][y + 2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                availableCheckMoves.add(new XY(1, 2));
            }
        }
        if ((x + 2) >= 0 && (x + 2) <= 7 && (y + 1) >= 0 && (y + 1) <= 7) {
            if (pieces[x + 2][y + 1] == null || !(pieces[x + 2][y + 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                availableCheckMoves.add(new XY(2, 1));
            }
        }
        if ((x + 2) >= 0 && (x + 2) <= 7 && (y - 1) >= 0 && (y - 1) <= 7) {
            if (pieces[x + 2][y - 1] == null || !(pieces[x + 2][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                availableCheckMoves.add(new XY(2, -1));
            }
        }
        if ((x + 1) >= 0 && (x + 1) <= 7 && (y - 2) >= 0 && (y - 2) <= 7) {
            if (pieces[x + 1][y - 2] == null || !(pieces[x + 1][y - 2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                availableCheckMoves.add(new XY(1, -2));
            }
        }
        if ((x - 1) >= 0 && (x - 1) <= 7 && (y - 2) >= 0 && (y - 2) <= 7) {
            if (pieces[x - 1][y - 2] == null || !(pieces[x - 1][y - 2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                availableCheckMoves.add(new XY(-1, -2));
            }
        }
        if ((x - 2) >= 0 && (x - 2) <= 7 && (y - 1) >= 0 && (y - 1) <= 7) {
            if (pieces[x - 2][y - 1] == null || !(pieces[x - 2][y - 1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor()))) {
                availableCheckMoves.add(new XY(-2, -1));
            }
        }
        return availableCheckMoves;
    }

    @Override
    public ArrayList getAvailableMovesListIgnoreKingAndColor(String color, int x, int y, Piece[][] pieces) {
        availableMoves.clear();

        if ((x - 2) >= 0 && (x - 2) <= 7 && (y + 1) >= 0 && (y + 1) <= 7) //if(pieces[x-2][y+1]==null || !(pieces[x-2][y+1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())))
        {
            availableMoves.add(new XY(-2, 1));
        }
        if ((x - 1) >= 0 && (x - 1) <= 7 && (y + 2) >= 0 && (y + 2) <= 7) //if(pieces[x-1][y+2]==null || !(pieces[x-1][y+2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())))
        {
            availableMoves.add(new XY(-1, 2));
        }
        if ((x + 1) >= 0 && (x + 1) <= 7 && (y + 2) >= 0 && (y + 2) <= 7) //if(pieces[x+1][y+2]==null || !(pieces[x+1][y+2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())))
        {
            availableMoves.add(new XY(1, 2));
        }
        if ((x + 2) >= 0 && (x + 2) <= 7 && (y + 1) >= 0 && (y + 1) <= 7) // if(pieces[x+2][y+1]==null || !(pieces[x+2][y+1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())))
        {
            availableMoves.add(new XY(2, 1));
        }
        if ((x + 2) >= 0 && (x + 2) <= 7 && (y - 1) >= 0 && (y - 1) <= 7) //  if(pieces[x+2][y-1]==null || !(pieces[x+2][y-1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())))
        {
            availableMoves.add(new XY(2, -1));
        }
        if ((x + 1) >= 0 && (x + 1) <= 7 && (y - 2) >= 0 && (y - 2) <= 7) //   if(pieces[x+1][y-2]==null || !(pieces[x+1][y-2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())))
        {
            availableMoves.add(new XY(1, -2));
        }
        if ((x - 1) >= 0 && (x - 1) <= 7 && (y - 2) >= 0 && (y - 2) <= 7) //    if(pieces[x-1][y-2]==null || !(pieces[x-1][y-2].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())))
        {
            availableMoves.add(new XY(-1, -2));
        }
        if ((x - 2) >= 0 && (x - 2) <= 7 && (y - 1) >= 0 && (y - 1) <= 7) //   if(pieces[x-2][y-1]==null || !(pieces[x-2][y-1].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())))
        {
            availableMoves.add(new XY(-2, -1));
        }
        return availableMoves;
    }

}
