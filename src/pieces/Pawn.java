package pieces;

import java.awt.Dimension;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JLabel;

/**
 *
 * @author TOM-12 <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class Pawn extends JLabel implements Piece,Serializable{

    private final ArrayList<XY> availableCheckMoves = new ArrayList<>();
    private final ArrayList<XY> availableMoves = new ArrayList<>();
    private boolean moved;
    private final String name = "pawn";
    private final String fullName;
    private final String color;
    /**
     * if moved two sqaures from position
     */
    private boolean twoSquareMove;

    /**
     * Constructs pawn piece of specified color
     *
     * @param c color of pawn("w" if white "b" if black)
     */
    public Pawn(String c) {
        color = c;
        setMinimumSize(new Dimension(50, 50));
        setPreferredSize(new Dimension(50, 50));
        fullName = color.concat("_pawn");
        setName(name);
        setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/" + fullName + ".png")));
        moved = false;
        twoSquareMove = false;
    }

    @Override
    public void moved() {
        moved = true;
    }

    @Override
    public boolean isMoved() {
        return moved;
    }

    /**
     * returns if pawn moved two squares
     *
     * @return true if moved two squares in one move
     */
    public boolean isTwoSquareMoved() {
        return twoSquareMove;
    }

    /**
     * changes parametr twoSquareMoved if pawn moved two squares in one move
     */
    public void twoSquareMoved() {
        twoSquareMove = true;
    }

    @Override
    public String getPieceName() {
        return name;
    }

    @Override
    public boolean checkIfMovePlacesInCheck(int x1, int y1, int a, int b, Piece[][] pieces) {
        boolean fl = false;
        Piece[][] tmp = new Piece[8][8];
        ArrayList tmpMoveList=new ArrayList();
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                tmp[i][j] = pieces[i][j];
            }
        }
        tmp[x1 + a][y1 + b] = tmp[x1][y1];
        tmp[x1][y1] = null;
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                if (tmp[i][j] != null && !tmp[i][j].getPieceColor().equalsIgnoreCase(color)) {
                    if (!tmp[i][j].getPieceName().equalsIgnoreCase("king")) {
                        tmpMoveList = tmp[i][j].getAvailableCheckMovesList(tmp[i][j].getPieceColor(), i, j, tmp);
                    } else {
                         tmpMoveList.clear();
                    }
                    if (!tmpMoveList.isEmpty()) {
                        for (int ii = 0; ii <= tmpMoveList.size() - 1; ++ii) {
                            int wsplX;
                            wsplX = i + (((XY) tmpMoveList.get(ii)).getX());
                            int wsplY;
                            wsplY = j + (((XY) tmpMoveList.get(ii)).getY());
                            if ((wsplX <= 7 && wsplX >= 0) && (wsplY <= 7 && wsplY >= 0)){
                                
                                if (tmp[wsplX][wsplY] != null && tmp[wsplX][wsplY].getPieceFullName().equalsIgnoreCase("w_king") && tmp[i][j].getPieceColor().equalsIgnoreCase("b")) {
                                    fl = true;
                                    break;
                                }
                                if (tmp[wsplX][wsplY] != null && tmp[wsplX][wsplY].getPieceFullName().equalsIgnoreCase("b_king") && tmp[i][j].getPieceColor().equalsIgnoreCase("w")) {
                                    fl = true;
                                    break;
                                }
                            }
                        }
                    } else if (tmpMoveList.isEmpty()) {
                    }
                }
            }
        }
        return fl;
    }

    @Override
    public ArrayList getAvailableMovesList(String color, int x, int y, Piece[][] pieces) {

        availableMoves.clear();
        boolean flag;

        if (color.equalsIgnoreCase("w")) {
            if (x == 6 && moved == false) {
                if (pieces[5][y] == null) {
                    flag=checkIfMovePlacesInCheck(x,y,-1,0,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(-1, 0));
                    }
                }
                if (pieces[5][y] == null && pieces[4][y] == null) {
                    flag=checkIfMovePlacesInCheck(x,y,-2,0,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(-2, 0));
                    }
                }
                if ((y - 1) >= 0 && pieces[5][y - 1] != null && (pieces[5][y - 1].getPieceColor().equalsIgnoreCase("b"))) {
                    flag=checkIfMovePlacesInCheck(x,y,-1,-1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(-1, -1));
                    }
                }
                if ((y + 1) <= 7 && pieces[5][y + 1] != null && (pieces[5][y + 1].getPieceColor().equalsIgnoreCase("b"))) {
                    flag=checkIfMovePlacesInCheck(x,y,-1,+1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(-1, +1));
                    }
                }
            } else {
                if ((x - 1) >= 0 && pieces[x - 1][y] == null) {
                    flag=checkIfMovePlacesInCheck(x,y,-1,0,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(-1, 0));
                    }
                }
                if ((x - 1) >= 0 && (y - 1) >= 0 && pieces[x - 1][y - 1] != null && (pieces[x - 1][y - 1].getPieceColor().equalsIgnoreCase("b"))) {
                    flag=checkIfMovePlacesInCheck(x,y,-1,-1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(-1, -1));
                    }
                }
                if ((x - 1) >= 0 && (y + 1) <= 7 && pieces[x - 1][y + 1] != null && (pieces[x - 1][y + 1].getPieceColor().equalsIgnoreCase("b"))) {
                    flag=checkIfMovePlacesInCheck(x,y,-1,1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(-1, +1));
                    }
                }
                if (x == 3 && (y - 1) >= 0 && pieces[3][y - 1] != null && pieces[3][y - 1].getPieceFullName().equalsIgnoreCase("b_pawn") && ((Pawn) pieces[3][y - 1]).isTwoSquareMoved()) {
                    flag=checkIfMovePlacesInCheck(x,y,-1,-1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(-1, -1));
                    }
                }
                if (x == 3 && (y + 1) <= 7 && pieces[3][y + 1] != null && pieces[3][y + 1].getPieceFullName().equalsIgnoreCase("b_pawn") && ((Pawn) pieces[3][y + 1]).isTwoSquareMoved()) {
                    flag=checkIfMovePlacesInCheck(x,y,-1,1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(-1, 1));
                    }
                }
            }

        } else if (color.equalsIgnoreCase("b")) {
            if (x == 1 && moved == false) {
                if (pieces[2][y] == null) {
                    flag=checkIfMovePlacesInCheck(x,y,1,0,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(1, 0));
                    }
                }
                if (pieces[2][y] == null && pieces[3][y] == null) {
                    flag=checkIfMovePlacesInCheck(x,y,2,0,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(2, 0));
                    }
                }
                if ((y - 1) >= 0 && pieces[2][y - 1] != null && (pieces[2][y - 1].getPieceColor().equalsIgnoreCase("w"))) {
                    flag=checkIfMovePlacesInCheck(x,y,1,-1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(1, -1));
                    }
                }
                if ((y + 1) <= 7 && pieces[2][y + 1] != null && (pieces[2][y + 1].getPieceColor().equalsIgnoreCase("w"))) {
                    flag=checkIfMovePlacesInCheck(x,y,1,1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(1, +1));
                    }
                }
            } else {
                if ((x + 1) <= 7 && pieces[x + 1][y] == null) {
                    flag=checkIfMovePlacesInCheck(x,y,1,0,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(1, 0));
                    }
                }
                if ((x + 1) <= 7 && (y - 1) >= 0 && pieces[x + 1][y - 1] != null && (pieces[x + 1][y - 1].getPieceColor().equalsIgnoreCase("w"))) {
                    flag=checkIfMovePlacesInCheck(x,y,1,-1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(1, -1));
                    }
                }
                if ((x + 1) <= 7 && (y + 1) <= 7 && pieces[x + 1][y + 1] != null && (pieces[x + 1][y + 1].getPieceColor().equalsIgnoreCase("w"))) {
                    flag=checkIfMovePlacesInCheck(x,y,1,1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(1, +1));
                    }
                }
                if (x == 4 && (y - 1) >= 0 && pieces[4][y - 1] != null && pieces[4][y - 1].getPieceFullName().equalsIgnoreCase("w_pawn") && ((Pawn) pieces[4][y - 1]).isTwoSquareMoved()) {
                    flag=checkIfMovePlacesInCheck(x,y,1,-1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(1, -1));
                    }
                }
                if (x == 4 && (y + 1) <= 7 && pieces[4][y + 1] != null && pieces[4][y + 1].getPieceFullName().equalsIgnoreCase("w_pawn") && ((Pawn) pieces[4][y + 1]).isTwoSquareMoved()) {
                    flag=checkIfMovePlacesInCheck(x,y,1,1,pieces);
                    if (flag == false) {
                        availableMoves.add(new XY(1, 1));
                    }
                }
            }
        }
        return availableMoves;
    }

    @Override
    public String getPieceColor() {
        return color;
    }

    @Override
    public String getPieceFullName() {
        return fullName;
    }

    @Override
    public String getPieceANEnglish() {
        return "";
    }

    @Override
    public String getPieceANPolish() {
        return "";
    }

    @Override
    public ArrayList getAvailableCheckMovesList(String color, int x, int y, Piece[][] pieces) {

        availableCheckMoves.clear();

        if (color.equalsIgnoreCase("w")) {
            if ((y - 1) >= 0 && (x - 1) >= 0) {
                availableCheckMoves.add(new XY(-1, -1));
            }
            if ((y + 1) <= 7 && (x - 1) >= 0) {
                availableCheckMoves.add(new XY(-1, +1));
            }
        } else if (color.equalsIgnoreCase("b")) {
            if ((y - 1) >= 0 && (x + 1) <= 7) {
                availableCheckMoves.add(new XY(1, -1));
            }
            if ((y + 1) <= 7 && (x + 1) <= 7) {
                availableCheckMoves.add(new XY(1, +1));
            }
        }
        return availableCheckMoves;
    }

    @Override
    public ArrayList getAvailableMovesListIgnoreKingAndColor(String color, int x, int y, Piece[][] pieces) {
        availableMoves.clear();

        if (color.equalsIgnoreCase("w")) {
            if ((y - 1) >= 0 && (x - 1) >= 0) {
                availableMoves.add(new XY(-1, -1));
            }
            if ((y + 1) <= 7 && (x - 1) >= 0) {
                availableMoves.add(new XY(-1, +1));
            }
        } else if (color.equalsIgnoreCase("b")) {
            if ((y - 1) >= 0 && (x + 1) <= 7) {
                availableMoves.add(new XY(1, -1));
            }
            if ((y + 1) <= 7 && (x + 1) <= 7) {
                availableMoves.add(new XY(1, +1));
            }
        }
        return availableMoves;
    }

}
