package pieces;

import java.util.ArrayList;

/**
 * Interface for Pieces
 * 
 * @author TOM-12  <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public interface Piece {
    
    
    

    /**
     *  Returns an arrayList of of <b>{@link XY}</b> objects, which represents
     * coordinates from source x,y coordinates of available moves
     * 
     * @param color     Pieces color "w" or "b"
     * @param x         x coordinate of Piece
     * @param y         y coordinate of Piece
     * @param pieces    array of existing pieces on board
     * @return          arrayList of available moves
     */
    ArrayList getAvailableMovesList(String color,int x,int y,Piece[][] pieces);
    
    /**
     * 
     * @param color
     * @param x
     * @param y
     * @param pieces
     * @return arrayList of available moves
     */
    ArrayList getAvailableMovesListIgnoreKingAndColor(String color,int x,int y,Piece[][] pieces);
    
    /**
     * checks if moving piece from x1,y1 to a,b places this pieces color king in check
     * @param x1    x coordinate of Piece starting
     * @param y1    y coordinate of Piece starting
     * @param a     x coordinate of Piece where to move
     * @param b     y coordinate of Piece where to move
     * @param pieces    array of existing pieces on board
     * @return true if move places in check
     */
    boolean checkIfMovePlacesInCheck(int x1,int y1,int a,int b,Piece[][] pieces);
    /**
     *  Returns an arrayList of of <b>{@link XY}</b> objects, which represents
     * coordinates from source x,y coordinates of available check moves
     * 
     * @param color     Pieces color "w" or "b"
     * @param x         x coordinate of Piece
     * @param y         y coordinate of Piece
     * @param pieces    array of existing pieces on board
     * @return          arrayList of available attack moves
     */
    ArrayList getAvailableCheckMovesList(String color,int x,int y,Piece[][] pieces);
    
    /**
     *  Changes parameter moved if Piece was moved
     */
    void moved();

    /**
     *  Returns whether Piece was moved or not
     * @return      true - if Piece was moved. false if not moved
     */
    boolean isMoved();

    /**
     *  Returns Pieces name e.g.: "pawn"
     * @return Pieces name
     */
    String getPieceName();    

    /**
     *  Returns Pieces full name as 'color'_'name 'e.g.: "w_pawn"
     * @return Pieces full name
     */
    String getPieceFullName();

    /**
     *  Returns Pieces color e.g.: "w" or "b"
     * @return Pieces color
     */
    String getPieceColor();
    
    /**
     * Returns Algebraic notation name piece (English)
     * @return Returns Algebraic notation name piece (English)
     */
    String getPieceANEnglish();
    
    /**
     * Returns Algebraic notation name piece (Polish)
     * @return Returns Algebraic notation name piece (Polish)
     */
    String getPieceANPolish();
}
