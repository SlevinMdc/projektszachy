/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pieces;

import java.awt.Dimension;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JLabel;

/**
 *
 * @author TOM-12 <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class Queen extends JLabel implements Piece,Serializable{

    private final ArrayList<XY> availableCheckMoves = new ArrayList<>();
    private final ArrayList<XY> availableMoves = new ArrayList<>();
    private boolean moved;
    private final String name = "queen";
    private final String fullName;
    private final String color;
    private boolean obstacleNE;
    private boolean obstacleSE;
    private boolean obstacleSW;
    private boolean obstacleNW;
    private boolean obstacleN;
    private boolean obstacleE;
    private boolean obstacleS;
    private boolean obstacleW;

    
    /**
     * Constructs queen piece of specified color
     *
     * @param c color of queen piece("w" if white "b" if black)
     */
    public Queen(String c) {
        color = c;
        setMinimumSize(new Dimension(50, 50));
        setPreferredSize(new Dimension(50, 50));
        fullName = color.concat("_queen");
        setName(name);
        setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/" + fullName + ".png")));
        moved = false;
    }
   
    @Override
    public boolean checkIfMovePlacesInCheck(int x1, int y1, int a, int b, Piece[][] pieces) {
        boolean fl = false;
        Piece[][] tmp = new Piece[8][8];
        ArrayList tmpMoveList=new ArrayList();
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                tmp[i][j] = pieces[i][j];
            }
        }
        tmp[x1 + a][y1 + b] = tmp[x1][y1];
        tmp[x1][y1] = null;
        for (int i = 0; i <= 7; ++i) {
            for (int j = 0; j <= 7; ++j) {
                if (tmp[i][j] != null && !tmp[i][j].getPieceColor().equalsIgnoreCase(color)) {
                    if (!tmp[i][j].getPieceName().equalsIgnoreCase("king")) {
                        tmpMoveList = tmp[i][j].getAvailableCheckMovesList(tmp[i][j].getPieceColor(), i, j, tmp);
                    } else {
                        tmpMoveList.clear();
                    }
                    if (!tmpMoveList.isEmpty()) {
                        for (int ii = 0; ii <= tmpMoveList.size() - 1; ++ii) {
                            int wsplX;
                            wsplX = i + (((XY) tmpMoveList.get(ii)).getX());
                            int wsplY;
                            wsplY = j + (((XY) tmpMoveList.get(ii)).getY());
                            if ((wsplX <= 7 && wsplX >= 0) && (wsplY <= 7 && wsplY >= 0)) {
                                if (tmp[wsplX][wsplY] != null && tmp[wsplX][wsplY].getPieceFullName().equalsIgnoreCase("w_king") && tmp[i][j].getPieceColor().equalsIgnoreCase("b")) {
                                    fl = true;
                                    break;
                                }
                                if (tmp[wsplX][wsplY] != null && tmp[wsplX][wsplY].getPieceFullName().equalsIgnoreCase("b_king") && tmp[i][j].getPieceColor().equalsIgnoreCase("w")) {
                                    fl = true;
                                    break;
                                }
                            }
                        }
                    } else if (tmpMoveList.isEmpty()) {
                    }
                }
            }
        }
        return fl;
    }

    @Override
    public ArrayList getAvailableMovesList(String color, int x, int y, Piece[][] pieces) {
        availableMoves.clear();
        obstacleNE = false;
        obstacleSE = false;
        obstacleSW = false;
        obstacleNW = false;
        obstacleN = false;
        obstacleE = false;
        obstacleS = false;
        obstacleW = false;
        boolean flag;

        for (int i = 1; i <= 7; ++i) {
            if ((x - i) >= 0 && (x - i) <= 7 && (y + i) <= 7 && (y + i) >= 0) {
                if (obstacleNE == false) {
                    if ((pieces[x - i][y + i] == null)) {
                        flag = checkIfMovePlacesInCheck(x, y, -i, +i, pieces);
                        if (flag == false) {
                            availableMoves.add(new XY(-i, +i));
                        }
                    } else {
                        if (pieces[x - i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleNE = true;
                        }
                        if (!pieces[x - i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            flag = checkIfMovePlacesInCheck(x, y, -i, +i, pieces);
                            obstacleNE = true;
                            if (flag == false) {
                                availableMoves.add(new XY(-i, +i));
                            }
                        }
                    }
                }
            }

            if ((x + i) >= 0 && (x + i) <= 7 && (y + i) <= 7 && (y + i) >= 0) {
                if (obstacleSE == false) {
                    if ((pieces[x + i][y + i] == null)) {
                        flag = checkIfMovePlacesInCheck(x, y, +i, +i, pieces);
                        if (flag == false) {
                            availableMoves.add(new XY(+i, +i));
                        }
                    } else {
                        if (pieces[x + i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleSE = true;
                        }
                        if (!pieces[x + i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            flag = checkIfMovePlacesInCheck(x, y, +i, +i, pieces);
                            obstacleSE = true;
                            if (flag == false) {
                                availableMoves.add(new XY(+i, +i));
                            }
                        }
                    }
                }
            }

            if ((x + i) >= 0 && (x + i) <= 7 && (y - i) <= 7 && (y - i) >= 0) {
                if (obstacleSW == false) {
                    if ((pieces[x + i][y - i] == null)) {
                        flag = checkIfMovePlacesInCheck(x, y, +i, -i, pieces);
                        if (flag == false) {
                            availableMoves.add(new XY(+i, -i));
                        }
                    } else {
                        if (pieces[x + i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleSW = true;
                        }
                        if (!pieces[x + i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            flag = checkIfMovePlacesInCheck(x, y, +i, -i, pieces);
                            obstacleSW = true;
                            if (flag == false) {
                                availableMoves.add(new XY(+i, -i));                                
                            }
                        }
                    }
                }
            }

            if ((x - i) >= 0 && (x - i) <= 7 && (y - i) <= 7 && (y - i) >= 0) {
                if (obstacleNW == false) {
                    if ((pieces[x - i][y - i] == null)) {
                        flag = checkIfMovePlacesInCheck(x, y, -i, -i, pieces);
                        if (flag == false) {
                            availableMoves.add(new XY(-i, -i));
                        }
                    } else {
                        if (pieces[x - i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleNW = true;
                        }
                        if (!pieces[x - i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            flag = checkIfMovePlacesInCheck(x, y, -i, -i, pieces);  
                            obstacleNW = true;
                            if (flag == false) {
                                availableMoves.add(new XY(-i, -i));
                            }
                        }
                    }
                }
            }
            //n e s w 
            if ((x - i) >= 0 && (x - i) <= 7) {
                if (obstacleN == false) {
                    if ((pieces[x - i][y] == null)) {
                        flag = checkIfMovePlacesInCheck(x, y, -i, 0, pieces);
                        if (flag == false) {
                            availableMoves.add(new XY(-i, 0));
                        }
                    } else {
                        if (pieces[x - i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleN = true;
                        }
                        if (!pieces[x - i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            flag = checkIfMovePlacesInCheck(x, y, -i, 0, pieces);
                            obstacleN = true;
                            if (flag == false) {
                                availableMoves.add(new XY(-i, 0));                                
                            }
                        }
                    }
                }
            }

            if ((y + i) <= 7 && (y + i) >= 0) {
                if (obstacleE == false) {
                    if ((pieces[x][y + i] == null)) {
                        flag = checkIfMovePlacesInCheck(x, y, 0, +i, pieces);
                        if (flag == false) {
                            availableMoves.add(new XY(0, +i));
                        }
                    } else {
                        if (pieces[x][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleE = true;
                        }
                        if (!pieces[x][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            flag = checkIfMovePlacesInCheck(x, y, 0, +i, pieces);
                            obstacleE = true;
                            if (flag == false) {                                
                                availableMoves.add(new XY(0, +i));                                
                            }
                        }
                    }
                }
            }

            if ((x + i) >= 0 && (x + i) <= 7) {
                if (obstacleS == false) {
                    if ((pieces[x + i][y] == null)) {
                        flag = checkIfMovePlacesInCheck(x, y, +i, 0, pieces);
                        if (flag == false) {
                            availableMoves.add(new XY(+i, 0));
                        }
                    } else {
                        if (pieces[x + i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleS = true;
                        }
                        if (!pieces[x + i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            flag = checkIfMovePlacesInCheck(x, y, +i, 0, pieces);
                            obstacleS = true;
                            if (flag == false) {
                                availableMoves.add(new XY(+i, 0));                                
                            }
                        }
                    }
                }
            }

            if ((y - i) <= 7 && (y - i) >= 0) {
                if (obstacleW == false) {
                    if ((pieces[x][y - i] == null)) {
                        flag = checkIfMovePlacesInCheck(x, y, 0, -i, pieces);
                        if (flag == false) {
                            availableMoves.add(new XY(0, -i));
                        }
                    } else {
                        if (pieces[x][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleW = true;
                        }
                        if (!pieces[x][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            flag = checkIfMovePlacesInCheck(x, y, 0, -i, pieces);
                            obstacleW = true;
                            if (flag == false) {
                                availableMoves.add(new XY(0, -i));                                
                            }
                        }
                    }
                }
            }
        }

        return availableMoves;
    }

    @Override
    public void moved() {
        moved = true;
    }

    @Override
    public boolean isMoved() {
        return moved;
    }

    @Override
    public String getPieceName() {
        return name;
    }

    @Override
    public String getPieceColor() {
        return color;
    }

    @Override
    public String getPieceFullName() {
        return fullName;
    }

    @Override
    public String getPieceANEnglish() {
        return "Q";
    }

    @Override
    public String getPieceANPolish() {
        return "H";
    }

    @Override
    public ArrayList getAvailableCheckMovesList(String color, int x, int y, Piece[][] pieces) {
        availableCheckMoves.clear();
        obstacleNE = false;
        obstacleSE = false;
        obstacleSW = false;
        obstacleNW = false;
        obstacleN = false;
        obstacleE = false;
        obstacleS = false;
        obstacleW = false;

        for (int i = 1; i <= 7; ++i) {
            if ((x - i) >= 0 && (x - i) <= 7 && (y + i) <= 7 && (y + i) >= 0) {
                if (obstacleNE == false) {
                    if ((pieces[x - i][y + i] == null)) {
                        availableCheckMoves.add(new XY(-i, +i));
                    } else {
                        if (pieces[x - i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleNE = true;
                        }
                        if (!pieces[x - i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableCheckMoves.add(new XY(-i,+i));
                            obstacleNE=true;
                        }
                    }
                }
            }

            if ((x + i) >= 0 && (x + i) <= 7 && (y + i) <= 7 && (y + i) >= 0) {
                if (obstacleSE == false) {
                    if ((pieces[x + i][y + i] == null)) {
                        availableCheckMoves.add(new XY(+i, +i));
                    } else {
                        if (pieces[x + i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleSE = true;
                        }
                        if (!pieces[x + i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableCheckMoves.add(new XY(+i,+i));
                            obstacleSE=true;
                        }
                    }
                }
            }

            if ((x + i) >= 0 && (x + i) <= 7 && (y - i) <= 7 && (y - i) >= 0) {
                if (obstacleSW == false) {
                    if ((pieces[x + i][y - i] == null)) {
                        availableCheckMoves.add(new XY(+i, -i));
                    } else {
                        if (pieces[x + i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleSW=true;
                        }
                        if (!pieces[x + i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableCheckMoves.add(new XY(+i,-i));
                            obstacleSW=true;
                        }
                    }
                }
            }

            if ((x - i) >= 0 && (x - i) <= 7 && (y - i) <= 7 && (y - i) >= 0) {
                if (obstacleNW == false) {
                    if ((pieces[x - i][y - i] == null)) {
                        availableCheckMoves.add(new XY(-i, -i));
                    } else {
                        if (pieces[x - i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleNW=true;
                        }
                        if (!pieces[x - i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableCheckMoves.add(new XY(-i,-i));
                            obstacleNW=true;
                        }
                    }
                }
            }
            //n e s w 
            if ((x - i) >= 0 && (x - i) <= 7) {
                if (obstacleN == false) {
                    if ((pieces[x - i][y] == null)) {
                        availableCheckMoves.add(new XY(-i, 0));
                    } else {
                        if (pieces[x - i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleN=true;
                        }
                        if (!pieces[x - i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableCheckMoves.add(new XY(-i,0));
                            obstacleN=true;
                        }
                    }
                }
            }

            if ((y + i) <= 7 && (y + i) >= 0) {
                if (obstacleE == false) {
                    if ((pieces[x][y + i] == null)) {
                        availableCheckMoves.add(new XY(0, +i));
                    } else {
                        if (pieces[x][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleE=true;
                        }
                        if (!pieces[x][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableCheckMoves.add(new XY(0,+i));
                            obstacleE=true;
                        }
                    }
                }
            }

            if ((x + i) >= 0 && (x + i) <= 7) {
                if (obstacleS == false) {
                    if ((pieces[x + i][y] == null)) {
                        availableCheckMoves.add(new XY(+i, 0));
                    } else {
                        if (pieces[x + i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleS=true;
                        }
                        if (!pieces[x + i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableCheckMoves.add(new XY(+i,0));
                            obstacleS=true;
                        }
                    }
                }
            }

            if ((y - i) <= 7 && (y - i) >= 0) {
                if (obstacleW == false) {
                    if ((pieces[x][y - i] == null)) {
                        availableCheckMoves.add(new XY(0, -i));
                    } else {
                        if (pieces[x][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            obstacleW=true;
                        }
                        if (!pieces[x][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableCheckMoves.add(new XY(0,-i));
                            obstacleW=true;
                        }
                    }
                }
            }
        }

        return availableCheckMoves;
    }

    @Override
    public ArrayList getAvailableMovesListIgnoreKingAndColor(String color, int x, int y, Piece[][] pieces) {
        availableMoves.clear();
        obstacleNE = false;
        obstacleSE = false;
        obstacleSW = false;
        obstacleNW = false;
        obstacleN = false;
        obstacleE = false;
        obstacleS = false;
        obstacleW = false;

        for (int i = 1; i <= 7; ++i) {
            if ((x - i) >= 0 && (x - i) <= 7 && (y + i) <= 7 && (y + i) >= 0) {
                if (obstacleNE == false) {
                    if ((pieces[x - i][y + i] == null)) {
                        availableMoves.add(new XY(-i, +i));
                    } else {
                        if (pieces[x - i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(-i, +i));
                            obstacleNE = true;
                        }
                        if (!pieces[x - i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(-i, +i));
                            if (!pieces[x - i][y + i].getPieceName().equalsIgnoreCase("king")) {
                                obstacleNE = true;
                            }
                        }
                    }
                }
            }

            if ((x + i) >= 0 && (x + i) <= 7 && (y + i) <= 7 && (y + i) >= 0) {
                if (obstacleSE == false) {
                    if ((pieces[x + i][y + i] == null)) {
                        availableMoves.add(new XY(+i, +i));
                    } else {
                        if (pieces[x + i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(+i, +i));
                            obstacleSE = true;
                        }
                        if (!pieces[x + i][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(+i, +i));
                            if (!pieces[x + i][y + i].getPieceName().equalsIgnoreCase("king")) {
                                obstacleSE = true;
                            }
                        }
                    }
                }
            }

            if ((x + i) >= 0 && (x + i) <= 7 && (y - i) <= 7 && (y - i) >= 0) {
                if (obstacleSW == false) {
                    if ((pieces[x + i][y - i] == null)) {
                        availableMoves.add(new XY(+i, -i));
                    } else {
                        if (pieces[x + i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(+i, -i));
                            obstacleSW = true;
                        }
                        if (!pieces[x + i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(+i, -i));
                            if (!pieces[x + i][y - i].getPieceName().equalsIgnoreCase("king")) {
                                obstacleSW = true;
                            }
                        }
                    }
                }
            }

            if ((x - i) >= 0 && (x - i) <= 7 && (y - i) <= 7 && (y - i) >= 0) {
                if (obstacleNW == false) {
                    if ((pieces[x - i][y - i] == null)) {
                        availableMoves.add(new XY(-i, -i));
                    } else {
                        if (pieces[x - i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(-i, -i));
                            obstacleNW = true;
                        }
                        if (!pieces[x - i][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(-i, -i));
                            if (!pieces[x - i][y - i].getPieceName().equalsIgnoreCase("king")) {
                                obstacleNW = true;
                            }
                        }
                    }
                }
            }
            //n e s w 
            if ((x - i) >= 0 && (x - i) <= 7) {
                if (obstacleN == false) {
                    if ((pieces[x - i][y] == null)) {
                        availableMoves.add(new XY(-i, 0));
                    } else {
                        if (pieces[x - i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(-i, 0));
                            obstacleN = true;
                        }
                        if (!pieces[x - i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(-i, 0));
                            if (!pieces[x - i][y].getPieceName().equalsIgnoreCase("king")) {
                                obstacleN = true;
                            }
                        }
                    }
                }
            }

            if ((y + i) <= 7 && (y + i) >= 0) {
                if (obstacleE == false) {
                    if ((pieces[x][y + i] == null)) {
                        availableMoves.add(new XY(0, +i));
                    } else {
                        if (pieces[x][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(0, +i));
                            obstacleE = true;
                        }
                        if (!pieces[x][y + i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(0, +i));
                            if (!pieces[x][y + i].getPieceName().equalsIgnoreCase("king")) {
                                obstacleE = true;
                            }
                        }
                    }
                }
            }

            if ((x + i) >= 0 && (x + i) <= 7) {
                if (obstacleS == false) {
                    if ((pieces[x + i][y] == null)) {
                        availableMoves.add(new XY(+i, 0));
                    } else {
                        if (pieces[x + i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(+i, 0));
                            obstacleS = true;
                        }
                        if (!pieces[x + i][y].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(+i, 0));
                            if (!pieces[x + i][y].getPieceName().equalsIgnoreCase("king")) {
                                obstacleS = true;
                            }
                        }
                    }
                }
            }

            if ((y - i) <= 7 && (y - i) >= 0) {
                if (obstacleW == false) {
                    if ((pieces[x][y - i] == null)) {
                        availableMoves.add(new XY(0, -i));
                    } else {
                        if (pieces[x][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(0, -i));
                            obstacleW = true;
                        }
                        if (!pieces[x][y - i].getPieceColor().equalsIgnoreCase(pieces[x][y].getPieceColor())) {
                            availableMoves.add(new XY(0, -i));
                            if (!pieces[x][y - i].getPieceName().equalsIgnoreCase("king")) {
                                obstacleW = true;
                            }
                        }
                    }
                }
            }
        }

        return availableMoves;

    }

}
