package pieces;

import java.io.Serializable;

/**
 * Class designed for containig in arrayLists set of 2 integers (x,y)
 * 
 * @author TOM-12  <a href="mailto:tomasz12@gmail.com">tomasz12@gmail.com</a>
 */
public class XY implements Serializable{
    /** first coordinate*/
    private int x;
    /**second coordinate*/
    private int y;

    /**
     *  Constructs objects XY which holds two coordinates(x,y)
     * 
     * @param x first coordinate
     * @param y second coordinate
     */
    public XY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     *  Returns parametr x
     * 
     * @return x
     */
    public int getX() {
        return x;
    }

    /**
     *  Sets x as first coordinate
     * 
     * @param x first coordinate
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Returns parametr x
     *
     * @return y
     */
    public int getY() {
        return y;
    }

    /**
     *  Sets y as first coordinate
     * 
     * @param y second coordinate
     */
    public void setY(int y) {
        this.y = y;
    }
    
}
